<?php


namespace App\Models;


use App\Core\Database\Database;
use App\Models\Staff\HpeTeacher;
use App\Models\Staff\MasterInCharge;
use stdClass;

class Event implements IModel
{
    private const TABLE = "events";

    public ?int $id, $hpe_teacher_id, $master_in_charge_id;
    public ?string $title;

    public ?MasterInCharge $master_in_charge; //masterInCharge
    public ?HpeTeacher $hpe_teacher;


    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    /**
     * @param int $id
     * @return self|null
     */
    public static function find( int $id ): ?self
    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->hpe_teacher = HpeTeacher::find( $result->hpe_teacher_id );
            $result->master_in_charge = MasterInCharge::find( $result->master_in_charge_id );
            return $result;
        }
        return null;
    }


    /**
     * @param int $limit
     * @param int $offset
     * @return self[]
     */
    public static function findAll( $limit = 1000, $offset = 0 ): array
    {
        /** @var self[] $results */
        $results = Database::findAll( self::TABLE, $limit, $offset, self::class, "id" );

        if ( !empty( $results ) ) {

            $output = [];

            foreach ( $results as $result ) {
                $result->hpe_teacher = HpeTeacher::find( $result->hpe_teacher_id );
                $result->master_in_charge = MasterInCharge::find( $result->master_in_charge_id );
                $output[] = $result;
            }

            return $output;
        }
        return [];
    }


    /**
     * @return int
     */
    public function insert(): int
    {
        $data = [
            "title" => $this->title,
            "master_in_charge_id" => $this->master_in_charge_id,
            "hpe_teacher_id" => $this->hpe_teacher_id,
        ];

        return Database::insert( self::TABLE, $data );

    }


    /**
     * @return bool
     */
    public function update(): bool
    {
        $data = [
            "title" => $this->title,
            "master_in_charge_id" => $this->master_in_charge_id,
            "hpe_teacher_id" => $this->hpe_teacher_id,
        ];


        return Database::update( self::TABLE, $data, [ "id" => $this->id ] );
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        return Database::delete( self::TABLE, "id", $this->id );
    }


    /**
     * Get the count of total rows
     * @return int
     */
    public static function getCount(): int
    {
        $db = Database::instance();
        $statement = $db->prepare( "select count(id) as total from events" );
        $statement->execute();

        $result = $statement->fetchObject( stdClass::class );

        if ( !empty( $result ) )
            return (int)$result->total;

        return 0;
    }
}
