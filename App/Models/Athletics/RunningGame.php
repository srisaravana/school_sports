<?php

namespace App\Models\Athletics;

use App\Core\Database\Database;
use App\Models\IModel;
use App\Models\Staff\HpeTeacher;
use App\Models\Staff\MasterInCharge;
use stdClass;

class RunningGame implements IModel
{

    public const TABLE = 'athletic_running_games';

    public ?int $id, $master_in_charge_id, $hpe_teacher_id;
    public ?int $title;

    public ?MasterInCharge $master_in_charge;
    public ?HpeTeacher $hpe_teacher;


    /**
     * @param $array
     * @return self
     */
    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    public static function find( int $id ): ?RunningGame
    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->hpe_teacher = HpeTeacher::find( $result->hpe_teacher_id );
            $result->master_in_charge = MasterInCharge::find( $result->master_in_charge_id );

            return $result;
        }

        return null;

    }

    public static function findAll( $limit = 1000, $offset = 0 ): array
    {
        /** @var self[] $results */
        $results = Database::findAll( self::TABLE, $limit, $offset, self::class, 'title' );

        if ( !empty( $results ) ) {

            foreach ( $results as $result ) {
                $result->hpe_teacher = HpeTeacher::find( $result->hpe_teacher_id );
                $result->master_in_charge = MasterInCharge::find( $result->master_in_charge_id );
            }

            return $results;
        }

        return [];

    }

    public function insert(): int
    {
        $data = [
            "title" => $this->title,
            "master_in_charge_id" => $this->master_in_charge_id,
            "hpe_teacher_id" => $this->hpe_teacher_id,
        ];

        return Database::insert( self::TABLE, $data );
    }

    public function update(): bool
    {
        $data = [
            "title" => $this->title,
            "master_in_charge_id" => $this->master_in_charge_id,
            "hpe_teacher_id" => $this->hpe_teacher_id,
        ];


        return Database::update( self::TABLE, $data, [ "id" => $this->id ] );
    }

    public function delete(): bool
    {
        return Database::delete( self::TABLE, "id", $this->id );
    }

    /**
     * Get the count of total rows
     * @return int
     */
    public static function getCount(): int
    {
        $db = Database::instance();
        $statement = $db->prepare( "select count(id) as total from athletic_running_games" );
        $statement->execute();

        $result = $statement->fetchObject( stdClass::class );

        if ( !empty( $result ) )
            return (int)$result->total;

        return 0;
    }
}
