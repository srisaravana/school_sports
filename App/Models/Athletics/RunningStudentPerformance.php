<?php

namespace App\Models\Athletics;

use App\Core\Database\Database;
use App\Models\Student;
use PDO;

class RunningStudentPerformance implements \App\Models\IModel
{

    public const TABLE = "running_students_performances";

    public ?int $id, $student_id;

    public ?string $meet_date, $meet_title, $meet_venue, $place;
    public ?float $timing;

    public ?Student $student;

    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }


    public static function find( int $id ): ?RunningStudentPerformance
    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->student = Student::find( $result->student_id );

            return $result;
        }

        return null;

    }

    public static function findAll( $limit = 1000, $offset = 0 )
    {
        // TODO: Implement findAll() method.
    }

    public function insert(): int
    {
        $data = [
            'student_id' => $this->student_id,
            'meet_date' => $this->meet_date,
            'meet_title' => $this->meet_title,
            'meet_venue' => $this->meet_venue,
            'place' => $this->place,
            'timing' => $this->timing,
        ];

        return Database::insert( self::TABLE, $data );
    }

    public function update(): bool
    {
        $data = [
            'meet_date' => $this->meet_date,
            'meet_title' => $this->meet_title,
            'meet_venue' => $this->meet_venue,
            'place' => $this->place,
            'timing' => $this->timing,
        ];

        return Database::update( self::TABLE, $data, [ 'id' => $this->id ] );
    }

    public function delete(): bool
    {
        return Database::delete( self::TABLE, 'id', $this->id );
    }


    public static function findByStudent( Student $student ): array
    {
        $db = Database::instance();
        $statement = $db->prepare( 'select * from running_students_performances where student_id=? order by meet_date' );
        $statement->execute( [ $student->id ] );

        /** @var self[] $results */
        $results = $statement->fetchAll( PDO::FETCH_CLASS, self::class );

        if ( !empty( $results ) ) {

            foreach ( $results as $result ) {
                $result->student = $student;
            }
            return $results;
        }
        return [];
    }

}
