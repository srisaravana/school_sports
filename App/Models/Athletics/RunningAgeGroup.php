<?php

namespace App\Models\Athletics;

use App\Core\Database\Database;
use App\Models\IModel;
use Exception;
use PDO;
use PDOException;

class RunningAgeGroup implements IModel
{

    public const TABLE = 'athletic_running_age_groups';

    public ?int $id, $running_game_id, $year;
    public ?string $title;

    public ?RunningGame $running_game;
    public ?RunningPractice $practice;


    /**
     * @param $array
     * @return self
     */
    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }


    public static function find( int $id ): ?RunningAgeGroup
    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->running_game = RunningGame::find( $result->running_game_id );
            $result->practice = RunningPractice::findByAgeGroup( $result->id );
            return $result;
        }

        return null;

    }

    public static function findAll( $limit = 1000, $offset = 0 ): array
    {
        /** @var self[] $results */
        $results = Database::findAll( self::TABLE, $limit, $offset, self::class, 'year' );

        if ( !empty( $results ) ) {

            foreach ( $results as $result ) {
                $result->running_game = RunningGame::find( $result->running_game_id );
            }

            return $results;
        }
        return [];

    }

    /**
     * @throws Exception
     */
    public function insert(): int
    {

        if ( $this->alreadyExists() ) throw new Exception( 'Already exist' );


        try {

            Database::instance()->beginTransaction();

            $data = [
                'running_game_id' => $this->running_game_id,
                'title' => $this->title,
                'year' => $this->year,
            ];

            $id = Database::insert( self::TABLE, $data );

            $ageGroup = RunningAgeGroup::find( $id );
            if ( empty( $ageGroup ) ) throw new Exception( 'Failed inserting new age group' );

            Database::insert( 'athletic_running_practices', [
                'age_group_id' => $id,
            ] );


            Database::instance()->commit();

            return $id;

        } catch ( PDOException $exception ) {

            Database::instance()->rollBack();
            return -1;

        } catch ( Exception $exception ) {
            throw new Exception( $exception );
        }

    }

    public function update(): bool
    {
        $data = [
            'title' => $this->title,
            'year' => $this->year,
        ];

        return Database::update( self::TABLE, $data, [ 'id' => $this->id ] );

    }

    public function delete(): bool
    {
        return Database::delete( self::TABLE, 'id', $this->id );
    }


    public function alreadyExists(): bool
    {
        $db = Database::instance();
        $statement = $db->prepare( 'select * from athletic_running_age_groups where title=? and year=? and running_game_id=? limit 1' );
        $statement->execute( [
            $this->title,
            $this->year,
            $this->running_game_id,
        ] );

        $result = $statement->fetchObject( self::class );

        if ( !empty( $result ) ) return true;
        return false;


    }

    public static function findByGame( RunningGame $runningGame ): array
    {
        $db = Database::instance();
        $statement = $db->prepare( 'select * from athletic_running_age_groups where running_game_id=?' );
        $statement->execute( [
            $runningGame->id,
        ] );

        $results = $statement->fetchAll( PDO::FETCH_CLASS, self::class );

        if ( !empty( $results ) ) {

            foreach ( $results as $result ) {
                $result->running_game = RunningGame::find( $result->running_game_id );
            }

            return $results;
        }
        return [];

    }

}
