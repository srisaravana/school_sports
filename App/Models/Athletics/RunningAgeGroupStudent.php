<?php

namespace App\Models\Athletics;

use App\Core\Database\Database;
use App\Models\IModel;
use App\Models\Student;
use Exception;
use PDO;

class RunningAgeGroupStudent implements IModel
{

    public const TABLE = "athletic_running_age_group_students";

    public ?int $id, $student_id, $age_group_id;

    public ?Student $student;
    public ?RunningAgeGroup $age_group;


    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    public static function find( int $id ): ?RunningAgeGroupStudent
    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->student = Student::find( $result->student_id );
            $result->age_group = RunningAgeGroup::find( $result->age_group_id );

            return $result;
        }
        return null;
    }

    public static function findAll( $limit = 1000, $offset = 0 )
    {
        // TODO: Implement findAll() method.
    }

    /**
     * @throws Exception
     */
    public function insert(): int
    {

        if ( $this->exists() ) throw new Exception( 'Already exist' );

        $data = [
            'age_group_id' => $this->age_group_id,
            'student_id' => $this->student_id,
        ];

        return Database::insert( self::TABLE, $data );
    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public function delete(): bool
    {
        return Database::delete( self::TABLE, 'id', $this->id );
    }


    public static function findByAgeGroup( RunningAgeGroup $runningAgeGroup ): array
    {
        $db = Database::instance();
        $statement = $db->prepare( 'select * from athletic_running_age_group_students where age_group_id=?' );
        $statement->execute( [ $runningAgeGroup->id ] );

        /** @var self[] $results */
        $results = $statement->fetchAll( PDO::FETCH_CLASS, self::class );


        if ( !empty( $results ) ) {
            foreach ( $results as $result ) {
                $result->student = Student::find( $result->student_id );
                $result->age_group = RunningAgeGroup::find( $result->age_group_id );
            }

            return $results;
        }
        return [];
    }


    public function exists(): bool
    {
        $db = Database::instance();
        $statement = $db->prepare( 'select * from athletic_running_age_group_students where student_id=? and age_group_id=? limit 1' );

        $statement->execute( [ $this->student_id, $this->age_group_id ] );

        $result = $statement->fetchObject( self::class );

        if ( !empty( $result ) ) return true;
        return false;

    }
}
