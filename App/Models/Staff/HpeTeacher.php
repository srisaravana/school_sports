<?php


namespace App\Models\Staff;


use App\Core\Database\Database;
use App\Models\IModel;

class HpeTeacher implements IModel
{

    private const TABLE = "hpe_teachers";
    public ?int $id;
    public ?string $full_name, $contact_number, $email, $address, $profile_pic;


    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    public static function find( int $id ): ?self
    {
        return Database::find( self::TABLE, $id, self::class );
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return self[]
     */
    public static function findAll( $limit = 1000, $offset = 0 ): array
    {
        return Database::findAll( self::TABLE, $limit, $offset, self::class, "full_name" );
    }

    public function insert(): int
    {
        $data = [
            "full_name" => $this->full_name,
            "contact_number" => $this->contact_number,
            "email" => $this->email,
            "address" => $this->address,
        ];

        return Database::insert( self::TABLE, $data );
    }

    public function update(): bool
    {
        $data = [
            "full_name" => $this->full_name,
            "contact_number" => $this->contact_number,
            "email" => $this->email,
            "address" => $this->address,
        ];

        return Database::update( self::TABLE, $data, [ "id" => $this->id ] );
    }


    public function updateProfilePicture(): bool
    {
        $data = [
            "profile_pic" => $this->profile_pic
        ];

        return Database::update( self::TABLE, $data, [ "id" => $this->id ] );
    }

    public function removeProfilePicture(): bool
    {
        $data = [
            "profile_pic" => ""
        ];

        return Database::update( self::TABLE, $data, [ "id" => $this->id ] );
    }

    public function delete(): bool
    {
        return Database::delete( self::TABLE, "id", $this->id );
    }
}
