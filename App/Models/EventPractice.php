<?php


namespace App\Models;


use App\Core\Database\Database;

class EventPractice implements IModel
{

    private const TABLE = "event_practices";

    public ?int $id, $event_age_group_id;
    public ?string $place, $monday, $tuesday, $wednesday, $thursday, $friday, $saturday, $sunday;

    public ?EventAgeGroup $event_age_group;


    /**
     * @param $array
     * @return self
     */
    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }


    /**
     * @param int $id
     * @return EventPractice|null
     */
    public static function find( int $id ): ?EventPractice
    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->event_age_group = EventAgeGroup::find( $result->event_age_group_id );
            return $result;
        }
        return null;
    }


    public static function findAll( $limit = 1000, $offset = 0 )
    {
        // TODO: Implement findAll() method.
    }


    /**
     * @return bool
     */
    public function insert(): bool
    {
        $data = [
            "event_age_group_id" => $this->event_age_group_id,
        ];
        return Database::insert( self::TABLE, $data );
    }


    /**
     * @return bool
     */
    public function update(): bool
    {
        $data = [
            "place" => $this->place,
            "monday" => $this->monday,
            "tuesday" => $this->tuesday,
            "wednesday" => $this->wednesday,
            "thursday" => $this->thursday,
            "friday" => $this->friday,
            "saturday" => $this->saturday,
            "sunday" => $this->sunday,
        ];
        return Database::update( self::TABLE, $data, [ "id" => $this->id ] );
    }


    /**
     *
     */
    public function delete()
    {
        // TODO: Implement delete() method.
    }


    /**
     * @param int $age_group_id
     * @return self|null
     */
    public static function findByAgeGroup( int $age_group_id ): ?self
    {
        $db = Database::instance();
        $statement = $db->prepare( 'select * from event_practices where event_age_group_id = ?' );
        $statement->execute( [ $age_group_id ] );

        $result = $statement->fetchObject( self::class );
        if ( !empty( $result ) ) return $result;
        return null;

    }

}
