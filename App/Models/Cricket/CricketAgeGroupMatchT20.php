<?php

namespace App\Models\Cricket;

use App\Core\Database\Database;
use App\Models\IModel;
use PDO;
use stdClass;


class CricketAgeGroupMatchT20 implements IModel
{
    private const TABLE = 'cricket_matches_t20';


    public const MATCH_TYPES = [
        '1D' => 'One Day',
        '2D' => 'Two Day',
        'T20' => 'T 20',
    ];

    public const RESULTS = [
        'WIN' => 'Win',
        'LOSS' => 'Loss',
        'TIE' => 'Tie',
        'DRAW' => 'Draw',
        'NO' => 'No Result',
    ];


    public ?int $id, $age_group_id, $a_runs, $a_wickets, $b_runs, $b_wickets;
    public ?string $match_date, $ground, $tournament_name, $result, $b_team_name, $status;

    public ?CricketAgeGroup $age_group;


    /**
     * Build an object from request array
     * @param $array
     * @return static
     */
    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    /**
     * @param int $id
     * @return self|null
     */
    public static function find( int $id ): ?self
    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->age_group = CricketAgeGroup::find( $result->age_group_id );
            return $result;
        }

        return null;
    }

    public static function findAll( $limit = 1000, $offset = 0 )
    {
        // TODO: Implement findAll() method.
    }

    /**
     * @return int
     */
    public function insert(): int
    {
        $data = [
            'age_group_id' => $this->age_group_id,

            'tournament_name' => $this->tournament_name,
            'match_date' => $this->match_date,
            'ground' => $this->ground,
            'b_team_name' => $this->b_team_name,

        ];

        return Database::insert( self::TABLE, $data );
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        $data = [
            'tournament_name' => $this->tournament_name,
            'match_date' => $this->match_date,
            'ground' => $this->ground,

            'a_runs' => $this->a_runs,
            'a_wickets' => $this->a_wickets,

            'b_team_name' => $this->b_team_name,
            'b_runs' => $this->b_runs,
            'b_wickets' => $this->b_wickets,

            'result' => $this->result,
            'status' => $this->status,
        ];

        return Database::update( self::TABLE, $data, [ 'id' => $this->id ] );
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        return Database::delete( self::TABLE, 'id', $this->id );
    }


    public static function findByAgeGroup( CricketAgeGroup $cricketAgeGroup ): array
    {
        $db = Database::instance();
        $statement = $db->prepare( 'select * from cricket_matches_t20 where age_group_id = ?' );

        $statement->execute( [ $cricketAgeGroup->id ] );

        /** @var self[] $results */
        $results = $statement->fetchAll( PDO::FETCH_CLASS, self::class );

        if ( !empty( $results ) ) {
            return $results;
        }

        return [];
    }


    /**
     * Get overall stats for the given age group
     *
     * @param CricketAgeGroup $cricketAgeGroup
     * @return array
     */
    public static function getStats( CricketAgeGroup $cricketAgeGroup ): array
    {

        $matchStats = self::getMatchStats( $cricketAgeGroup->id );
        $runsStats = self::getGamesRunsStats( $cricketAgeGroup->id );

        return [
            'matches' => $matchStats,
            'games_runs' => $runsStats,
        ];

    }

    /*
     * Private functions: Stats calculations
     * */

    /**
     * Get played matches outcomes
     *
     * @param int $ageGroupId
     * @return int[]
     */
    private static function getMatchStats( int $ageGroupId ): array
    {
        $db = Database::instance();

        $output = [
            'win' => 0,
            'loss' => 0,
            'tie' => 0,
            'draw' => 0,
            'no' => 0,
        ];

        /*
         * Get total wins
         * */
        $statement = $db->prepare( 'select count(id) as win from cricket_matches_t20 where result="WIN" AND age_group_id = ? AND status="COMPLETED"' );
        $statement->execute( [ $ageGroupId ] );
        $result = $statement->fetchObject( stdClass::class );
        $output[ 'win' ] = (int)$result->win;

        $statement = $db->prepare( 'select count(id) as loss from cricket_matches_t20 where result="LOSS" AND age_group_id = ? AND status="COMPLETED"' );
        $statement->execute( [ $ageGroupId ] );
        $result = $statement->fetchObject( stdClass::class );
        $output[ 'loss' ] = (int)$result->loss;

        $statement = $db->prepare( 'select count(id) as tie from cricket_matches_t20 where result="TIE" AND age_group_id = ? AND status="COMPLETED"' );
        $statement->execute( [ $ageGroupId ] );
        $result = $statement->fetchObject( stdClass::class );
        $output[ 'tie' ] = (int)$result->tie;

        $statement = $db->prepare( 'select count(id) as draw from cricket_matches_t20 where result="DRAW" AND age_group_id = ? AND status="COMPLETED"' );
        $statement->execute( [ $ageGroupId ] );
        $result = $statement->fetchObject( stdClass::class );
        $output[ 'draw' ] = (int)$result->draw;

        $statement = $db->prepare( 'select count(id) as no from cricket_matches_t20 where result="NO" AND age_group_id = ? AND status="COMPLETED"' );
        $statement->execute( [ $ageGroupId ] );
        $result = $statement->fetchObject( stdClass::class );
        $output[ 'no' ] = (int)$result->no;

        return $output;

    }

    /**
     * Get all games runs and names
     *
     * @param int $ageGroupId
     * @return array
     */
    private static function getGamesRunsStats( int $ageGroupId ): array
    {

        $db = Database::instance();
        $statement = $db->prepare( 'select * from cricket_matches_t20 where age_group_id = ? AND status="COMPLETED"' );
        $statement->execute( [ $ageGroupId ] );

        /** @var self[] $results */
        $results = $statement->fetchAll( PDO::FETCH_CLASS, self::class );

        $output = [
            'games_titles' => [],
            'team_a_runs' => [],
            'team_b_runs' => [],
        ];


        foreach ( $results as $result ) {
            $output[ 'games_titles' ][] = $result->tournament_name;
            $output[ 'team_a_runs' ][] = $result->a_runs;
            $output[ 'team_b_runs' ][] = $result->b_runs;
        }


        return $output;

    }


}
