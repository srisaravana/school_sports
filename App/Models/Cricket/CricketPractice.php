<?php

namespace App\Models\Cricket;

use App\Core\Database\Database;
use App\Models\IModel;

class CricketPractice implements IModel
{
    private const TABLE = "cricket_practices";

    public ?int $id, $age_group_id;
    public ?string $place, $monday, $tuesday, $wednesday, $thursday, $friday, $saturday, $sunday;

    public ?CricketAgeGroup $age_group;


    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    public static function find( int $id ): ?CricketPractice
    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->age_group = CricketAgeGroup::find( $result->age_group_id );
            return $result;
        }
        return null;
    }

    public static function findAll( $limit = 1000, $offset = 0 )
    {
        // TODO: Implement findAll() method.
    }

    public function insert(): int
    {
        $data = [
            "age_group_id" => $this->age_group_id,
        ];
        return Database::insert( self::TABLE, $data );
    }

    public function update(): bool
    {
        $data = [
            "place" => $this->place,
            "monday" => $this->monday,
            "tuesday" => $this->tuesday,
            "wednesday" => $this->wednesday,
            "thursday" => $this->thursday,
            "friday" => $this->friday,
            "saturday" => $this->saturday,
            "sunday" => $this->sunday,
        ];
        return Database::update( self::TABLE, $data, [ "id" => $this->id ] );
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }


    /**
     * @param int $age_group_id
     * @return self|null
     */
    public static function findByAgeGroup( int $age_group_id ): ?self
    {
        $db = Database::instance();
        $statement = $db->prepare( 'select * from cricket_practices where age_group_id = ?' );
        $statement->execute( [ $age_group_id ] );

        $result = $statement->fetchObject( self::class );
        if ( !empty( $result ) ) return $result;
        return null;

    }

}
