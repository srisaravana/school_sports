<?php

namespace App\Models\Cricket;

use App\Core\Database\Database;
use App\Models\IModel;
use App\Models\Student;
use PDO;

class CricketStudentPerformance2Days implements IModel
{

    private const TABLE = 'cricket_students_performances_2days';

    public ?int $id, $student_id, $match_id;
    public ?string $performance_type;
    public ?int $batting_runs_1, $batting_balls_faced_1, $batting_4s_1, $batting_6s_1;
    public ?int $batting_runs_2, $batting_balls_faced_2, $batting_4s_2, $batting_6s_2;
    public ?int $bowling_balls_1, $bowling_runs_1, $bowling_wickets_1, $bowling_wides_1, $bowling_noballs_1;
    public ?int $bowling_balls_2, $bowling_runs_2, $bowling_wickets_2, $bowling_wides_2, $bowling_noballs_2;

    public ?float $bowling_overs_1, $bowling_overs_2;

    public ?Student $student;
    public ?CricketAgeGroupMatch2Days $ageGroupMatch;


    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    /**
     * @param int $id
     * @return self|null
     */
    public static function find( int $id ): ?self

    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->student = Student::find( $result->student_id );
            $result->ageGroupMatch = CricketAgeGroupMatch2Days::find( $result->match_id );

            return $result;
        }

        return null;
    }

    public static function findAll( $limit = 1000, $offset = 0 )
    {
        // TODO: Implement findAll() method.
    }


    /**
     * Inserts a new student performance record.
     * @return int
     */
    public function insert(): int
    {
        $data = [
            'student_id' => $this->student_id,
            'match_id' => $this->match_id,
            'performance_type' => $this->performance_type,
            'batting_runs_1' => $this->batting_runs_1,
            'batting_runs_2' => $this->batting_runs_2,
            'batting_balls_faced_1' => $this->batting_balls_faced_1,
            'batting_balls_faced_2' => $this->batting_balls_faced_2,
            'batting_4s_1' => $this->batting_4s_1,
            'batting_4s_2' => $this->batting_4s_2,
            'batting_6s_1' => $this->batting_6s_1,
            'batting_6s_2' => $this->batting_6s_2,
            'bowling_overs_1' => $this->bowling_overs_1,
            'bowling_overs_2' => $this->bowling_overs_2,
            'bowling_balls_1' => $this->bowling_balls_1,
            'bowling_balls_2' => $this->bowling_balls_2,
            'bowling_runs_1' => $this->bowling_runs_1,
            'bowling_runs_2' => $this->bowling_runs_2,
            'bowling_wickets_1' => $this->bowling_wickets_1,
            'bowling_wickets_2' => $this->bowling_wickets_2,
            'bowling_wides_1' => $this->bowling_wides_1,
            'bowling_wides_2' => $this->bowling_wides_2,
            'bowling_noballs_1' => $this->bowling_noballs_1,
            'bowling_noballs_2' => $this->bowling_noballs_2,
        ];

        return Database::insert( self::TABLE, $data );
    }

    public function update(): bool
    {
        $data = [
            'batting_runs_1' => $this->batting_runs_1,
            'batting_runs_2' => $this->batting_runs_2,
            'batting_balls_faced_1' => $this->batting_balls_faced_1,
            'batting_balls_faced_2' => $this->batting_balls_faced_2,
            'batting_4s_1' => $this->batting_4s_1,
            'batting_4s_2' => $this->batting_4s_2,
            'batting_6s_1' => $this->batting_6s_1,
            'batting_6s_2' => $this->batting_6s_2,
            'bowling_overs_1' => $this->bowling_overs_1,
            'bowling_overs_2' => $this->bowling_overs_2,
            'bowling_balls_1' => $this->bowling_balls_1,
            'bowling_balls_2' => $this->bowling_balls_2,
            'bowling_runs_1' => $this->bowling_runs_1,
            'bowling_runs_2' => $this->bowling_runs_2,
            'bowling_wickets_1' => $this->bowling_wickets_1,
            'bowling_wickets_2' => $this->bowling_wickets_2,
            'bowling_wides_1' => $this->bowling_wides_1,
            'bowling_wides_2' => $this->bowling_wides_2,
            'bowling_noballs_1' => $this->bowling_noballs_1,
            'bowling_noballs_2' => $this->bowling_noballs_2,
        ];

        return Database::update( self::TABLE, $data, [ 'id' => $this->id ] );

    }

    public function delete(): bool
    {
        return Database::delete( self::TABLE, 'id', $this->id );
    }


    public static function findByStudent( Student $student ): array
    {
        $db = Database::instance();
        $statement = $db->prepare( 'SELECT * from cricket_students_performances_2days where student_id=?' );

        $statement->execute( [ $student->id ] );

        /** @var self[] $results */
        $results = $statement->fetchAll( PDO::FETCH_CLASS, self::class );


        if ( !empty( $results ) ) {

            foreach ( $results as $result ) {
                $result->ageGroupMatch = CricketAgeGroupMatch2Days::find( $result->match_id );
            }

            return $results;
        }

        return [];

    }
}
