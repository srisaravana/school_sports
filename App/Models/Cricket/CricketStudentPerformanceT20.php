<?php

namespace App\Models\Cricket;

use App\Core\Database\Database;
use App\Models\IModel;
use App\Models\Student;
use PDO;

class CricketStudentPerformanceT20 implements IModel
{

    private const TABLE = 'cricket_students_performances_t20';

    public ?int $id, $student_id, $match_id;
    public ?string $performance_type;
    public ?int $batting_runs, $batting_balls_faced, $batting_4s, $batting_6s;
    public ?int $bowling_balls, $bowling_runs, $bowling_wickets, $bowling_wides, $bowling_noballs;

    public ?float $bowling_overs;

    public ?Student $student;
    public ?CricketAgeGroupMatchT20 $ageGroupMatch;


    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    /**
     * @param int $id
     * @return self|null
     */
    public static function find( int $id ): ?self
    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->student = Student::find( $result->student_id );
            $result->ageGroupMatch = CricketAgeGroupMatchT20::find( $result->match_id );

            return $result;
        }

        return null;
    }

    public static function findAll( $limit = 1000, $offset = 0 )
    {
        // TODO: Implement findAll() method.
    }


    /**
     * Inserts a new student performance record.
     * @return int
     */
    public function insert(): int
    {
        $data = [
            'student_id' => $this->student_id,
            'match_id' => $this->match_id,
            'performance_type' => $this->performance_type,
            'batting_runs' => $this->batting_runs,
            'batting_balls_faced' => $this->batting_balls_faced,
            'batting_4s' => $this->batting_4s,
            'batting_6s' => $this->batting_6s,
            'bowling_overs' => $this->bowling_overs,
            'bowling_balls' => $this->bowling_balls,
            'bowling_runs' => $this->bowling_runs,
            'bowling_wickets' => $this->bowling_wickets,
            'bowling_wides' => $this->bowling_wides,
            'bowling_noballs' => $this->bowling_noballs,
        ];

        return Database::insert( self::TABLE, $data );
    }

    public function update(): bool
    {
        $data = [
            'batting_runs' => $this->batting_runs,
            'batting_balls_faced' => $this->batting_balls_faced,
            'batting_4s' => $this->batting_4s,
            'batting_6s' => $this->batting_6s,
            'bowling_overs' => $this->bowling_overs,
            'bowling_balls' => $this->bowling_balls,
            'bowling_runs' => $this->bowling_runs,
            'bowling_wickets' => $this->bowling_wickets,
            'bowling_wides' => $this->bowling_wides,
            'bowling_noballs' => $this->bowling_noballs,
        ];

        return Database::update( self::TABLE, $data, [ 'id' => $this->id ] );

    }

    public function delete(): bool
    {
        return Database::delete( self::TABLE, 'id', $this->id );
    }


    public static function findByStudent( Student $student ): array
    {
        $db = Database::instance();
        $statement = $db->prepare( 'SELECT * from cricket_students_performances_t20 where student_id=?' );

        $statement->execute( [ $student->id ] );

        /** @var self[] $results */
        $results = $statement->fetchAll( PDO::FETCH_CLASS, self::class );


        if ( !empty( $results ) ) {

            foreach ( $results as $result ) {
                $result->ageGroupMatch = CricketAgeGroupMatchT20::find( $result->match_id );
            }

            return $results;
        }

        return [];

    }
}
