<?php

namespace App\Models\Cricket;

use App\Core\Database\Database;
use App\Models\IModel;
use App\Models\Staff\Coach;
use App\Models\Staff\MasterInCharge;
use Exception;
use PDOException;

class CricketAgeGroup implements IModel
{

    public const TABLE = 'cricket_age_groups';

    public ?int $id, $coach_id, $master_in_charge_id, $age;
    public ?string $title, $year;

    public ?Coach $coach;
    public ?MasterInCharge $master_in_charge;
    public ?CricketPractice $practice;

    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    public static function find( int $id ): ?CricketAgeGroup
    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->coach = Coach::find( $result->coach_id );
            $result->master_in_charge = MasterInCharge::find( $result->master_in_charge_id );
            $result->practice = CricketPractice::findByAgeGroup( $result->id );
            return $result;
        }

        return null;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return self[]
     */
    public static function findAll( $limit = 1000, $offset = 0 ): array
    {
        /** @var self[] $results */
        $results = Database::findAll( self::TABLE, $limit, $offset, self::class, 'year' );

        $output = [];
        if ( !empty( $results ) ) {
            foreach ( $results as $result ) {
                $result->coach = Coach::find( $result->coach_id );
                $result->master_in_charge = MasterInCharge::find( $result->master_in_charge_id );
                $output[] = $result;
            }
        }
        return $output;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function insert(): int
    {

        $exist = self::ageGroupExist( $this->year, $this->title );
        if ( $exist !== null ) throw new Exception( 'Age group already exist' );

        try {

            Database::instance()->beginTransaction();

            $data = [
                'coach_id' => $this->coach_id,
                'master_in_charge_id' => $this->master_in_charge_id,
                'title' => $this->title,
                'year' => $this->year,
                'age' => $this->age,
            ];

            $id = Database::insert( self::TABLE, $data );

            $ageGroup = CricketAgeGroup::find( $id );
            if ( empty( $ageGroup ) ) throw new Exception( 'Failed inserting new age group' );

            /* add a new practice */
            Database::insert( 'cricket_practices', [
                'age_group_id' => $id,
            ] );

            Database::instance()->commit();

            return $id;


        } catch ( PDOException $exception ) {
            Database::instance()->rollBack();
            return -1;
        } catch ( Exception $exception ) {
            throw new Exception( $exception );
        }


    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        $data = [
            'coach_id' => $this->coach_id,
            'master_in_charge_id' => $this->master_in_charge_id,
            'title' => $this->title,
            'year' => $this->year,
            'age' => $this->age,
        ];

        return Database::update( self::TABLE, $data, [ 'id' => $this->id ] );
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        return Database::delete( self::TABLE, 'id', $this->id );
    }


    public function ageGroupExist( $year, $title ): ?CricketAgeGroup
    {
        $db = Database::instance();
        $statement = $db->prepare( 'select * from cricket_age_groups where title=? and year=? limit 1' );
        $statement->execute( [ $title, $year ] );

        /** @var self $result */
        $result = $statement->fetchObject( self::class );

        if ( !empty( $result ) ) return $result;
        return null;

    }

}
