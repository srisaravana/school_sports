<?php


namespace App\Models;


use App\Core\Database\Database;
use Carbon\Carbon;
use PDO;
use stdClass;

class Student implements IModel
{

    private const TABLE = "students";

    public ?int $id;
    public ?string $full_name, $dob, $house, $date_of_admission, $bc_number, $email, $address,
        $guardian_name, $contact_number, $grade, $admission_number, $profile_pic;


    public static function build($array): self
    {
        $object = new self();
        foreach ($array as $key => $value) {
            $object->$key = $value;
        }
        return $object;
    }

    /**
     * @param int $id
     * @return self|null
     */
    public static function find(int $id): ?self
    {
        return Database::find(self::TABLE, $id, self::class);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return self[]
     */
    public static function findAll($limit = 1000, $offset = 0): array
    {
        return Database::findAll(self::TABLE, $limit, $offset, self::class, "id");
    }

    /**
     * @return int
     */
    public function insert(): int
    {
        $data = [
            "admission_number" => $this->admission_number,
            "full_name" => $this->full_name,
            "dob" => $this->dob,
            "grade" => $this->grade,
            "house" => $this->house,
            "date_of_admission" => $this->date_of_admission,
            "bc_number" => $this->bc_number,
            "email" => $this->email,
            "contact_number" => $this->contact_number,
            "address" => $this->address,
            "guardian_name" => $this->guardian_name,
        ];

        return Database::insert(self::TABLE, $data);

    }

    public function update(): bool
    {
        $data = [
            "admission_number" => $this->admission_number,
            "full_name" => $this->full_name,
            "dob" => $this->dob,
            "grade" => $this->grade,
            "house" => $this->house,
            "date_of_admission" => $this->date_of_admission,
            "bc_number" => $this->bc_number,
            "email" => $this->email,
            "contact_number" => $this->contact_number,
            "address" => $this->address,
            "guardian_name" => $this->guardian_name,
        ];

        return Database::update(self::TABLE, $data, ["id" => $this->id]);
    }


    public function updateProfilePicture(): bool
    {
        $data = [
            "profile_pic" => $this->profile_pic
        ];

        return Database::update(self::TABLE, $data, ["id" => $this->id]);
    }


    public function removeProfilePicture(): bool
    {
        $data = [
            "profile_pic" => ""
        ];
        return Database::update(self::TABLE, $data, ["id" => $this->id]);
    }


    public function delete(): bool
    {
        return Database::delete(self::TABLE, "id", $this->id);
    }


    /**
     * @param string $keyword
     * @param ?int $age
     * @return Student[]
     */
    public static function search(string $keyword, ?int $age): array
    {

        $db = Database::instance();

        if (!empty($age)) {
            $today = (new Carbon())->startOfYear();
            $startYear = $today->subYears($age)->toDateString();

            $today = (new Carbon())->startOfYear();
            $endYear = $today->subYears($age - 1)->toDateString();


            $statement = $db->prepare("select * from students 
                                        where (full_name like :q or 
                                              admission_number like :q or
                                              address like :q or 
                                              date_of_admission like :q or
                                              house like :q or
                                              bc_number like :q or
                                              grade like :q) and
                                              dob between :startYear and :endYear
                                              limit 20");


            $statement->execute([
                ":q" => "%" . $keyword . "%",
                ":startYear" => $startYear,
                ":endYear" => $endYear,
            ]);
        } else {

            $statement = $db->prepare("select * from students 
                                        where (full_name like :q or 
                                              admission_number like :q or
                                              address like :q or 
                                              date_of_admission like :q or
                                              house like :q or
                                              bc_number like :q or
                                              grade like :q)                                            
                                              limit 20");


            $statement->execute([
                ":q" => "%" . $keyword . "%",
            ]);

        }

        /** @var self[] $result */
        $result = $statement->fetchAll(PDO::FETCH_CLASS, self::class);

        if (!empty($result)) return $result;
        return [];

    }


    public static function getCount(): int
    {
        $db = Database::instance();
        $statement = $db->prepare("select count(id) as total from students");
        $statement->execute();

        $result = $statement->fetchObject(stdClass::class);

        if (!empty($result))
            return (int)$result->total;

        return 0;
    }

}
