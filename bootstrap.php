<?php

declare( strict_types=1 );

require_once "vendor/autoload.php";


use App\Core\Database\Database;
use App\Core\Http\Request;
use App\Core\Services\Storage;

Request::cors();

$db_config = [
    "HOST" => "localhost",
    "DATABASE" => "school_sports",
    "USERNAME" => "root",
    "PASSWORD" => "",
];

Database::init( $db_config );

/* set storage configurations */
Storage::setUploadDir( __DIR__ . "/public" );
Storage::setMaxUploadSize( 5 * 1024 * 1024 ); // 5MB
