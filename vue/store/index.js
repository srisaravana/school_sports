import {ageGroupAchievementsStore} from '@src/store/modules/age_group_achievements';
import {runningStore} from '@src/store/modules/athletics_running.js';
import {authStore} from '@src/store/modules/auth';
import {coachesStore} from '@src/store/modules/coaches';
import {cricketAgeGroupsStore} from '@src/store/modules/cricket_age_groups';
import {cricketAgeGroupMatches1DayStore} from '@src/store/modules/cricket_matches_1day.js';
import {cricketAgeGroupMatches2DaysStore} from '@src/store/modules/cricket_matches_2days.js';
import {cricketAgeGroupMatchesT20Store} from '@src/store/modules/cricket_matches_t20.js';
import {cricketPerformances1DayStore} from '@src/store/modules/cricket_performances_1day.js';
import {cricketPerformances2DaysStore} from '@src/store/modules/cricket_performances_2days.js';
import {cricketPerformancesT20Store} from '@src/store/modules/cricket_performances_t20.js';
import {eventsStore} from '@src/store/modules/events.js';
import {eventAgeGroupsStore} from '@src/store/modules/events_age_groups.js';
import {hpeTeachersStore} from '@src/store/modules/hpe_teachers';
import {masterInChargeStore} from '@src/store/modules/masters_in_charge';
import {sportPracticesStore} from '@src/store/modules/sport_practices';
import {studentsStore} from '@src/store/modules/students';
import {usersStore} from '@src/store/modules/users';

import Vue from 'vue';
import Vuex from 'vuex';


Vue.use( Vuex );

export default new Vuex.Store( {
    namespaced: true,
    state: {},
    mutations: {},
    actions: {},
    modules: {
        auth: authStore,
        users: usersStore,
        students: studentsStore,

        coaches: coachesStore,
        hpeTeachers: hpeTeachersStore,
        mastersInCharge: masterInChargeStore,

        events: eventsStore,
        eventsAgeGroups: eventAgeGroupsStore,
        practices: sportPracticesStore,
        achievements: ageGroupAchievementsStore,

        cricketAgeGroups: cricketAgeGroupsStore,
        cricketMatches1Day: cricketAgeGroupMatches1DayStore,
        cricketMatches2Days: cricketAgeGroupMatches2DaysStore,
        cricketMatchesT20: cricketAgeGroupMatchesT20Store,

        /* students performances */
        cricketPerformances1Day: cricketPerformances1DayStore,
        cricketPerformances2Days: cricketPerformances2DaysStore,
        cricketPerformancesT20: cricketPerformancesT20Store,

        /* running */
        running: runningStore,

    },

} );
