import axios from 'axios';


export const runningStore = {

    namespaced: true,

    state: function () {

        return {
            selectedGame: {},
            gamesList: [],


            ageGroupsList: [],

        };
    },


    getters: {
        getSelectedGame( state ) { return state.selectedGame; },
        getGamesList( state ) { return state.gamesList; },
        getAgeGroupsList( state ) { return state.ageGroupsList; },
    },


    actions: {

        async fetchAll( context ) {

            const response = await axios.get( `running/all.php` );
            context.state.gamesList = response.data.payload[ 'games' ];

        }, /* fetch all */

        async fetch( context, id ) {
            const response = await axios.get( `running/get.php?id=${ id }` );
            context.state.selectedGame = response.data.payload.data;
        }, /* fetch */

        async create( context, params ) {
            await axios.post( 'running/create.php', params );
        }, /* create */

        async update( context, params ) {
            await axios.post( 'running/update.php', params );
        }, /* update */

        async createAgeGroup( context, params ) {
            await axios.post( 'running-age-groups/create.php', params );
        },

        async fetchAllAgeGroupsByGame( context, gameId ) {
            const response = await axios.post( 'running-age-groups/by-game.php', { game_id: gameId } );
            context.state.ageGroupsList = response.data.payload;
        },

        async fetchAgeGroup( context, id ) {
            const response = await axios.post( 'running-age-groups/get.php', { id: id } );
            return response.data.payload[ 'data' ];
        },


        /**
         *
         * @param context
         * @param params -[student_id, age_group_id]
         */
        async insertAgeGroupStudent( context, params ) {
            const response = await axios.post( 'running-age-groups/insert-student.php', params );
        },

        async fetchStudentsByAgeGroup( context, ageGroupId ) {
            const response = await axios.post( 'running-age-groups/all-students.php', { id: ageGroupId } );
            return response.data.payload;
        },

        async fetchAgeGroupStudent( context, ageGroupStudentId ) {
            const response = await axios.post( 'running-age-groups/get-age-group-student.php', { id: ageGroupStudentId } );
            return response.data.payload.data;
        },


        /*
         *
         * Student's performances
         *
         */

        async fetchAllStudentPerformances( context, studentId ) {
            const response = await axios.post( 'running-age-groups/get-all-student-performances.php', { student_id: studentId } );
            return response.data.payload;
        },

        async addStudentPerformance( context, params ) {
            await axios.post( 'running-age-groups/add-student-performance.php', params );
        },

        async removeStudentPerformance( context, id ) {
            await axios.post( 'running-age-groups/remove-student-performance.php', { id: id } );
        },

        async updateStudentPerformance( context, params ) {
            await axios.post( 'running-age-groups/update-student-performance.php', params );
        },


        async updatePractice( context, params ) {
            await axios.post( 'running-age-groups/update-practice.php', params );
        },

    },


};
