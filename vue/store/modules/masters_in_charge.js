import axios from 'axios';

export const masterInChargeStore = {

    namespaced: true,

    state: function () {
        return {

            /** @type {MasterInCharge[]} */
            mastersInChargeList: [],

            /** @type {MasterInCharge} */
            selectedMasterInCharge: {},

        };
    },

    getters: {
        /* Masters In Charge */
        getMastersInChargeList( state ) {
            return state.mastersInChargeList;
        },

        getSelectedMasterInCharge( state ) {
            return state.selectedMasterInCharge;
        },
    },

    actions: {

        async fetchAll( context ) {

            const response = await axios.get( `staff/masters-in-charge/all.php` );
            context.state.mastersInChargeList = response.data.payload;

        }, /* fetch all */

        async fetch( context, id ) {
            const response = await axios.get( `staff/masters-in-charge/get.php?id=${ id }` );
            context.state.selectedMasterInCharge = response.data.payload.data;

        }, /* fetch */

        async create( context, params ) {
            await axios.post( 'staff/masters-in-charge//create.php', params );
        }, /* create */

        async update( context, params ) {
            await axios.post( 'staff/masters-in-charge/update.php', params );
        }, /* update */

        /**
         * Upload Coach's profile picture
         * @param context
         * @param params
         */
        async uploadProfilePic( context, params ) {
            let formData = new FormData();
            formData.append( 'id', params.id );
            formData.append( 'profile_pic', params.profile_pic );

            await axios.post( 'staff/masters-in-charge/upload-profile-pic.php', formData, {
                'Content-Type': 'multipart/form-data',
            } );
        }, /* upload profile pic */

    },

};
