import axios from 'axios';


export const eventAgeGroupsStore = {

    namespaced: true,

    state: function () {

        return {};
    },


    getters: {},


    actions: {

        async fetchByEvent(context, eventId) {

            const response = await axios.get(`events-age-groups/by-event.php?id=${eventId}`);
            return response.data.payload;

        }, /* fetch all */

        async fetch(context, id) {
            const response = await axios.get(`events-age-groups/get.php?id=${id}`);
            return response.data.payload.data;
        }, /* fetch */

        async create(context, params) {
            await axios.post('events-age-groups/create.php', params);
        }, /* create */

        async update(context, params) {
            await axios.post('events-age-groups/update.php', params);
        }, /* update */

        /* ---------------------------------------------------------------------------------- */

        /*
        * Actions for age group students
        * fetch students associated with the given age group
        * add new student to an age group
        * */

        async fetchGroupStudents(context, ageGroupId) {
            const response = await axios.get(`events-age-groups/get-students.php?id=${ageGroupId}`);
            return response.data.payload;
        },

        async fetchAgeGroupStudent(context, ageGroupStudentId) {
            const response = await axios.post('events-age-groups/get-age-group-student.php', {id: ageGroupStudentId});
            return response.data.payload.data;
        },

        async addAgeGroupStudent(context, params) {
            await axios.post('events-age-groups/add-student.php', params);
        },

        async removeAgeGroupStudent(context, id) {
            await axios.post('events-age-groups/remove-student.php', {id: id});
        },

        async fetchAllForStudent(context, studentId) {
            const response = await axios.post('events-age-groups/all-by-student.php', {student_id: studentId});
            return response.data.payload;
        },


        /*
         *
         * Student's performances
         *
         */

        async fetchAllStudentPerformances(context, studentId) {
            const response = await axios.post('events-age-groups/get-all-student-performances.php', {student_id: studentId});
            return response.data.payload;
        },

        async addStudentPerformance(context, params) {
            await axios.post('events-age-groups/add-student-performance.php', params);
        },

        async removeStudentPerformance(context, id) {
            await axios.post('events-age-groups/remove-student-performance.php', {id: id});
        },

        async updateStudentPerformance(context, params) {
            await axios.post('events-age-groups/update-student-performance.php', params);
        },

    },


};
