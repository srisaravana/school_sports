import axios from 'axios';

export const sportPracticesStore = {

    state() {
        return {

            /** @type {SportPractice} */
            sportPractice: {},

        };
    },

    getters: {
        getSportPractice( state ) { return state.sportPractice; },
    },

    actions: {

        // async practices_fetch( context, id ) {
        //     const response = await axios.get( `practices/get.php?id=${ id }` );
        //     context.state.sportPractice = response.data.payload.data;
        // },

        async practices_update( context, params ) {
            await axios.post( 'practices/update.php', params );
        },

    },

};
