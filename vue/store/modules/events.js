import axios from 'axios';

export const eventsStore = {

    namespaced: true,

    actions: {

        async fetchAll( context ) {

            const response = await axios.get( `events/all.php` );
            return response.data.payload[ 'events' ];

        }, /* fetch all */

        async fetch( context, id ) {
            const response = await axios.get( `events/get.php?id=${ id }` );
            return response.data.payload[ 'data' ];
        }, /* fetch */

        async create( context, params ) {
            await axios.post( 'events/create.php', params );
        }, /* create */

        async update( context, params ) {
            await axios.post( 'events/update.php', params );
        }, /* update */

    },

};
