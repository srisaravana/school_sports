import axios from 'axios';

export const studentsStore = {

    namespaced: true,

    state() {
        return {
            studentsList: [],
            student: {},
            totalStudentsCount: 0,

        };
    },

    getters: {

        getStudentsList(state) {
            return state.studentsList;
        },

        getStudent(state) {
            return state.student;
        },

        getTotalStudentsCount(state) {
            return state.totalStudentsCount;
        },

    },

    mutations: {

        setStudentsList(state, data) {
            state.studentsList = data;
        },

        setStudent(state, data) {
            state.student = data;
        },

        setTotalStudentsCount(state, value) {
            state.totalStudentsCount = value;
        },

    },

    actions: {


        /* fetch */
        async fetch(context, id) {

            const response = await axios.get(`students/get.php?id=${id}`);
            context.commit('setStudent', response.data.payload.data);

        },

        /* fetch all */
        async fetchAll(context, params = {offset: 0, limit: 1000}) {

            const response = await axios.get(`students/all.php?limit=${params.limit}&offset=${params.offset}`);
            context.commit('setStudentsList', response.data.payload.students);
            context.commit('setTotalStudentsCount', response.data.payload.total);
        },

        /* create */
        async create(context, params) {

            await axios.post('students/create.php', params);
        },

        /* search */
        async search(context, params) {
            const response = await axios.post(`students/search.php`, params);
            context.commit('setStudentsList', response.data.payload);
        },


        /* update */
        async update(context, params) {
            await axios.post('students/update.php', params);
        },


        async uploadProfilePic(context, params = {profilePicFile: null, id: null}) {

            let formData = new FormData();
            formData.append('profile_pic', params.profilePicFile);
            formData.append('id', params.id);

            const response = await axios.post('students/upload-profile-pic.php', formData, {
                'Content-Type': 'multipart/form-data',
            });

        }, /* upload profile pic */

        async removeProfilePic(context, id) {
            await axios.post('students/remove-profile-pic.php', {id: id});
        }, /* remove profile pic */


    }, /* __actions__ */

};
