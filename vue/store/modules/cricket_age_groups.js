import axios from 'axios';

export const cricketAgeGroupsStore = {

    namespaced: true,

    state() {
        return {

            /** @type {CricketAgeGroup[]} */
            ageGroupsList: [],

            /** @type {CricketAgeGroup} */
            ageGroup: {},

            /** @type {CricketAgeGroupStudent[]} */
            cricketAgeGroupStudentsList: [],


            /*
            Selected cricket age group student to be used in CricketAgeGroupStudentPage
            */
            /** @type {CricketAgeGroupStudent} */
            ageGroupStudent: {},


        };
    },

    getters: {

        getAgeGroupsList(state) {
            return state.ageGroupsList;
        },

        getAgeGroup(state) {
            return state.ageGroup;
        },

        getStudentsListInAgeGroup(state) {
            return state.cricketAgeGroupStudentsList;
        },

        getAgeGroupStudent(state) {
            return state.ageGroupStudent;
        },

    },

    actions: {

        async fetchAll(context) {

            const response = await axios.get('cricket-age-groups/all.php');
            context.state.ageGroupsList = response.data.payload;
        },

        async create(context, params) {
            await axios.post('cricket-age-groups/create.php', params);
        },

        async delete(context, ageGroupId) {
            await axios.post('cricket-age-groups/delete.php', {id: ageGroupId});
        },

        async fetch(context, ageGroupId) {
            const response = await axios.post('cricket-age-groups/get.php', {id: ageGroupId});
            context.state.ageGroup = response.data.payload.data;
        },

        async update(context, params) {
            await axios.post('cricket-age-groups/update.php', params);
        },


        /*
        *
        * Cricket Age Group Students ----------------------
        *
        * */

        async addStudent(context, params) {
            await axios.post('cricket-age-groups/add-student.php', params);
        },

        async removeStudent(context, id) {
            await axios.post('cricket-age-groups/remove-student.php', {id: id});
        },

        async fetchAllAgeGroupStudents(context, ageGroupId) {
            const response = await axios.post('cricket-age-groups/all-students.php', {id: ageGroupId});
            context.state.cricketAgeGroupStudentsList = response.data.payload;
        },

        async fetchAgeGroupStudent(context, ageGroupStudentId) {
            const response = await axios.post('cricket-age-groups/get-student.php', {id: ageGroupStudentId});
            context.state.ageGroupStudent = response.data.payload.data;
        },

        async fetchAllForStudent(context, studentId) {
            const response = await axios.post('cricket-age-groups/all-by-student.php', {student_id: studentId});
            return response.data.payload;
        },

        /*
        *
        * Cricket Practice ---------------------------------
        *
        * */

        async updatePractice(context, params) {
            await axios.post('cricket-age-groups/update-practice.php', params);
        },


    },

};
