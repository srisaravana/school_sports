import axios from 'axios';

export const cricketPerformances1DayStore = {

    namespaced: true,

    state() {
        return {

            allPerformances: [],
            selectedPerformance: {},

        };
    },

    getters: {
        getAllPerformances( state ) {
            return state.allPerformances;
        },

        getSelectedPerformance( state ) {
            return state.selectedPerformance;
        },

    },

    actions: {

        /**
         * Fetch all 1 day performances by the student
         * @param context
         * @param studentId
         */
        async fetchAllByStudent( context, studentId ) {
            const response = await axios.post(
                'cricket-age-groups/students/get-by-student-1day.php',
                { student_id: studentId },
            );

            context.state.allPerformances = response.data.payload;
        },

        /**
         * Add new 1 day performance
         * @param context
         * @param params
         */
        async add( context, params ) {
            await axios.post( 'cricket-age-groups/students/add-performance-1day.php', params );
        },

        /**
         * Update selected 1 day performance
         * @param context
         * @param params
         */
        async update( context, params ) {
            await axios.post( 'cricket-age-groups/students/update-performance-1day.php', params );
        },

        /**
         * Deletes the performance
         * @param context
         * @param performanceId
         */
        async delete( context, performanceId ) {
            await axios.post( 'cricket-age-groups/students/delete-performance-1day.php', { id: performanceId } );
        },

    },

};
