import axios from 'axios';

export const hpeTeachersStore = {

    namespaced: true,

    state: function () {
        return {
            /** @type {HpeTeacher[]} */
            hpeTeachersList: [],

            /** @type {HpeTeacher} */
            selectedHpeTeacher: {},
        };
    },

    getters: {
        /* HPE teachers */
        getHpeTeachersList( state ) {
            return state.hpeTeachersList;
        },

        getSelectedHpeTeacher( state ) {
            return state.selectedHpeTeacher;
        },
    },

    actions: {

        /**
         * Fetch all HPE Teachers list
         * @param context
         */
        async fetchAll( context ) {

            const response = await axios.get( `staff/hpe-teachers/all.php` );
            context.state.hpeTeachersList = response.data.payload;
        }, /* fetch all */


        /**
         * Fetch selected HPE Teacher by ID
         * @param context
         * @param id
         */
        async fetch( context, id ) {
            const response = await axios.get( `staff/hpe-teachers/get.php?id=${ id }` );
            context.state.selectedHpeTeacher = response.data.payload.data;
        }, /* fetch */


        /**
         * Create new HPE Teacher
         * @param context
         * @param params
         */
        async create( context, params ) {
            await axios.post( 'staff/hpe-teachers/create.php', params );
        }, /* create */

        /**
         *
         * @param context
         * @param params
         */
        async update( context, params ) {
            await axios.post( 'staff/hpe-teachers/update.php', params );
        }, /* update */


        /**
         * Upload HPE Teacher's profile picture
         * @param context
         * @param params
         */
        async uploadProfilePic( context, params ) {
            let formData = new FormData();
            formData.append( 'id', params.id );
            formData.append( 'profile_pic', params.profile_pic );

            await axios.post( 'staff/hpe-teachers/upload-profile-pic.php', formData, {
                'Content-Type': 'multipart/form-data',
            } );
        }, /* upload profile pic */

    },

};
