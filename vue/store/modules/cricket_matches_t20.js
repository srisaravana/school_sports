import axios from 'axios';

export const cricketAgeGroupMatchesT20Store = {
    namespaced: true,

    state() {
        return {
            matchesList: [],
            match: {},
            stats: {},
        };
    },

    getters: {
        getMatchesList( state ) {
            return state.matchesList;
        },

        getMatch( state ) {
            return state.match;
        },

        getStats( state ) {
            return state.stats;
        },
    },

    actions: {
        /**
         * Add a new match record for the age group
         * @param context
         * @param params - age_group_id, tournament_name, match_date,
         *               - ground, a_runs, b_runs, a_wickets, b_wickets,
         *               - b_team_name, result
         */
        async addMatch( context, params ) {
            await axios.post( 'cricket-age-groups/matches/add-t20.php', params );
        },

        /**
         * Remove the match record from the age group
         * @param context
         * @param id
         */
        async removeMatch( context, id ) {
            await axios.post( 'cricket-age-groups/matches/remove-t20.php', { id: id } );
        },

        /**
         * Update the match record from the age group
         * @param context
         * @param params
         */
        async updateMatch( context, params ) {
            await axios.post( 'cricket-age-groups/matches/update-t20.php', params );
        },

        /**
         * Fetch all one day match records for the given age group
         * @param context
         * @param ageGroupId
         */
        async fetchAllMatches( context, ageGroupId ) {
            const response = await axios.post( 'cricket-age-groups/matches/all-t20.php', { id: ageGroupId } );
            context.state.matchesList = response.data.payload;
        },

        /**
         * Fetch selected one day match details
         * @param context
         * @param performanceId
         */
        async fetchMatch( context, performanceId ) {
            const response = await axios.post( 'cricket-age-groups/matches/get-t20.php', { id: performanceId } );
            context.state.match = response.data.payload.data;
        },


        /**
         * Get one day play statistics for the age group
         * @param context
         * @param ageGroupId
         * @return {Promise<void>}
         */
        async fetchStats( context, ageGroupId ) {
            const response = await axios.post( 'cricket-age-groups/matches/get-stats-t20.php', { id: ageGroupId } );
            context.state.stats = response.data.payload;
        },

    },

};
