import ManageRunningAgeGroupsPage from '@src/views/athletics/running/ManageRunningAgeGroupsPage.vue';
import ManageRunningGamesPage from '@src/views/athletics/running/ManageRunningGamesPage.vue';
import RunningAgeGroupPage from '@src/views/athletics/running/RunningAgeGroupPage.vue';
import RunningAgeGroupStudentPage from '@src/views/athletics/running/RunningAgeGroupStudentPage.vue';

export const RunningRoutes = [

    {
        path: '/athletics/running',
        name: 'manageRunningGamesPage',
        component: ManageRunningGamesPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/athletics/running/:gameId',
        name: 'manageRunningAgeGroupsPage',
        component: ManageRunningAgeGroupsPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/athletics/running/:gameId/:ageGroupId',
        name: 'runningAgeGroupPage',
        component: RunningAgeGroupPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/athletics/running/:gameId/:ageGroupId/:ageGroupStudentId',
        name: 'runningAgeGroupStudentPage',
        component: RunningAgeGroupStudentPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },

];
