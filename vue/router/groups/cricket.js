import CricketAgeGroupPage from '@src/views/cricket/CricketAgeGroupPage';
import CricketAgeGroupStudentPage from '@src/views/cricket/CricketAgeGroupStudentPage.vue';
import CricketPage from '@src/views/cricket/CricketPage';
import PrintStudent1DayPerformancePage from '@src/views/cricket/PrintStudent1DayPerformancePage.vue';
import PrintStudent2DaysPerformancePage from '@src/views/cricket/PrintStudent2DaysPerformancePage.vue';
import PrintStudentT20PerformancePage from '@src/views/cricket/PrintStudentT20PerformancePage.vue';

export const CricketRoutes = [
    {
        path: '/cricket',
        name: 'cricketPage',
        component: CricketPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/cricket/age-group/:ageGroupId',
        name: 'cricketAgeGroupPage',
        component: CricketAgeGroupPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/cricket/age-group/:ageGroupId/students/:ageGroupStudentId',
        name: 'CricketAgeGroupStudentPage',
        component: CricketAgeGroupStudentPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/cricket/age-group/:ageGroupId/students/:ageGroupStudentId/1day/print',
        name: 'CricketPrintOneDayPerformances',
        component: PrintStudent1DayPerformancePage,
    },
    {
        path: '/cricket/age-group/:ageGroupId/students/:ageGroupStudentId/2days/print',
        name: 'CricketPrintTwoDaysPerformances',
        component: PrintStudent2DaysPerformancePage,
    },
    {
        path: '/cricket/age-group/:ageGroupId/students/:ageGroupStudentId/t20/print',
        name: 'CricketPrintT20Performances',
        component: PrintStudentT20PerformancePage,
    },
];
