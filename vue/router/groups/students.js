import CreateStudentPage from '@src/views/students/CreateStudentPage';
import EditStudentPage from '@src/views/students/EditStudentPage';
import ListAllStudentsSubPage from '@src/views/students/list/ListAllStudentsSubPage';
import SearchStudentsSubPage from '@src/views/students/list/SearchStudentsSubPage';
import ListStudentsPage from '@src/views/students/ListStudentsPage';

export const StudentsRoutes = [

    {
        path: '/students',
        component: ListStudentsPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
        children: [
            {
                path: '',
                name: 'manageStudents',
                component: ListAllStudentsSubPage,
            },
            {
                path: 'search/:keyword',
                name: 'searchStudents',
                component: SearchStudentsSubPage,
            },
        ],

    },

    {
        path: '/st/edit/:id',
        name: 'editStudent',
        component: EditStudentPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },

    {
        path: '/students/create',
        name: 'createStudent',
        component: CreateStudentPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },


];
