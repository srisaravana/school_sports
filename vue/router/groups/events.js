import AgeGroupDetailsPage from '@src/views/events/age_groups/AgeGroupDetailsPage';
import AgeGroupStudentPage from '@src/views/events/age_groups/AgeGroupStudentPage.vue';
import EventDetailsPage from '@src/views/events/EventDetailsPage.vue';
import ManageEventsPage from '@src/views/events/ManageEventsPage.vue';

export const EventsRoutes = [
    {
        path: '/events',
        name: 'manageEventsPage',
        component: ManageEventsPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/events/:id',
        name: 'eventDetailsPage',
        component: EventDetailsPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/events/:eventId/age-group/:ageGroupId',
        name: 'ageGroupDetailsPage',
        component: AgeGroupDetailsPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/events/:eventId/:ageGroupId/:ageGroupStudentId',
        name: 'ageGroupStudentPage',
        component: AgeGroupStudentPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },

];
