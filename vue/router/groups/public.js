import CricketPracticeDetailsPage from '@src/views/public/sports/CricketPracticeDetailsPage.vue';
import EventPracticeDetailsPage from '@src/views/public/sports/EventPracticeDetailsPage.vue';
import RunningPracticeDetailsPage from '@src/views/public/sports/RunningPracticeDetailsPage.vue';

export const PublicRoutes = [
    {
        path: '/public/practice/running/:ageGroupId',
        name: 'publicRunningPractice',
        component: RunningPracticeDetailsPage,
        meta: {},
    },
    {
        path: '/public/practice/cricket/:ageGroupId',
        name: 'publicCricketPractice',
        component: CricketPracticeDetailsPage,
        meta: {},
    },
    {
        path: '/public/practice/event/:ageGroupId',
        name: 'publicEventPractice',
        component: EventPracticeDetailsPage,
        meta: {},
    },
];
