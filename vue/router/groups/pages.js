import About from '@src/views/pages/About.vue';
import Home from '@src/views/pages/Home.vue';

export const PagesRoutes = [

    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/about',
        name: 'about',
        component: About,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
];
