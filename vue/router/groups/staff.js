import EditCoachPage from '@src/views/staff/coaches/EditCoachPage';
import ManageCoachesPage from '@src/views/staff/coaches/ManageCoachesPage';
import EditHpeTeacherPage from '@src/views/staff/hpe_teachers/EditHpeTeacherPage';
import ManageHpeTeachersPage from '@src/views/staff/hpe_teachers/ManageHpeTeachersPage';
import EditMasterInChargePage from '@src/views/staff/masters_in_charge/EditMasterInChargePage';
import ManageMastersInChargePage from '@src/views/staff/masters_in_charge/ManageMastersInChargePage';
import ManageStaff from '../../views/staff/ManageStaff';

export const StaffRoutes = [

    /* Staff */
    {
        path: '/staff',
        name: 'manageStaffPage',
        component: ManageStaff,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    /* HPE teachers */
    {
        path: '/staff/hpe-teachers',
        name: 'manageHpeTeachersPage',
        component: ManageHpeTeachersPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/staff/hpe-teachers/edit/:id',
        name: 'editHpeTeacherPage',
        component: EditHpeTeacherPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },

    /* Masters in-charge */
    {
        path: '/staff/masters-in-charge',
        name: 'manageMastersInChargePage',
        component: ManageMastersInChargePage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/staff/masters-in-charge/edit/:id',
        name: 'editMasterInChargePage',
        component: EditMasterInChargePage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },

    /* coaches */
    {
        path: '/staff/coaches',
        name: 'manageCoachesPage',
        component: ManageCoachesPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/staff/coaches/edit/:id',
        name: 'editCoachPage',
        component: EditCoachPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },

];
