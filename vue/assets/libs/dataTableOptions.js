export const defaultDTOptions = {
    'responsive': true,
    'retrieve': true,
    'paging': false,
    'info': false,
    'language': {
        search: '<i class="bi bi-funnel-fill"></i>',
        searchPlaceholder: 'filter records',
    },
};

export const fullDTOptions = {
    'paging': true,
    'responsive': true,
    'retrieve': true,
    'info': true,
    'language': {
        search: '<i class="bi bi-funnel-fill"></i>',
        searchPlaceholder: 'filter records',
    },
};
