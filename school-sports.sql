-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.13-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6327
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for school_sports
DROP DATABASE IF EXISTS `school_sports`;
CREATE DATABASE IF NOT EXISTS `school_sports` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `school_sports`;

-- Dumping structure for table school_sports.coaches
DROP TABLE IF EXISTS `coaches`;
CREATE TABLE IF NOT EXISTS `coaches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `contact_number` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `coaching_license` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table school_sports.coaches: ~0 rows (approximately)
DELETE FROM `coaches`;
/*!40000 ALTER TABLE `coaches` DISABLE KEYS */;
INSERT INTO `coaches` (`id`, `full_name`, `contact_number`, `email`, `address`, `coaching_license`) VALUES
	(1, 'Ramanan K', '0784152165', 'ramanan.k@yahoo.com', '#23, Loothu matha str,\nBar Road, Batticaloa', '4578/745');
/*!40000 ALTER TABLE `coaches` ENABLE KEYS */;

-- Dumping structure for table school_sports.hpe_teachers
DROP TABLE IF EXISTS `hpe_teachers`;
CREATE TABLE IF NOT EXISTS `hpe_teachers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table school_sports.hpe_teachers: ~3 rows (approximately)
DELETE FROM `hpe_teachers`;
/*!40000 ALTER TABLE `hpe_teachers` DISABLE KEYS */;
INSERT INTO `hpe_teachers` (`id`, `full_name`, `contact_number`, `email`, `address`) VALUES
	(1, 'John Wick', '0774512631', 'john.wick@hello.com', 'Some place,\nSecret Bangalow,\nBatticaloa'),
	(2, 'Thanos', 'Galactic', 'thanos@titan.com', '#titan,\nsomeplace, dangerous'),
	(3, 'Tony', 'Stark', 'tony@avegners.com', '#Stark building,\nNy city,\nNy.'),
	(4, 'test 2', 'test 2', 'test 2', 'test\ntest2');
/*!40000 ALTER TABLE `hpe_teachers` ENABLE KEYS */;

-- Dumping structure for table school_sports.masters_in_charge
DROP TABLE IF EXISTS `masters_in_charge`;
CREATE TABLE IF NOT EXISTS `masters_in_charge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table school_sports.masters_in_charge: ~2 rows (approximately)
DELETE FROM `masters_in_charge`;
/*!40000 ALTER TABLE `masters_in_charge` DISABLE KEYS */;
INSERT INTO `masters_in_charge` (`id`, `full_name`, `contact_number`, `email`, `address`) VALUES
	(1, 'Kumaran Sakthivell', '077-1231231', 'kumaran@hello.com', '#23, someplace,\nBar road,\nBatticaloa'),
	(2, 'Kandeepan Sinnathambi', '077-0987123', 'kandi@yahoo.com', '#45, New some road,\nBatticaloa');
/*!40000 ALTER TABLE `masters_in_charge` ENABLE KEYS */;

-- Dumping structure for table school_sports.sports
DROP TABLE IF EXISTS `sports`;
CREATE TABLE IF NOT EXISTS `sports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `hpe_teacher_id` int(10) unsigned NOT NULL,
  `master_in_charge_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sports_hpe_teachers` (`hpe_teacher_id`),
  KEY `FK_sports_masters_in_charge` (`master_in_charge_id`),
  CONSTRAINT `FK_sports_hpe_teachers` FOREIGN KEY (`hpe_teacher_id`) REFERENCES `hpe_teachers` (`id`),
  CONSTRAINT `FK_sports_masters_in_charge` FOREIGN KEY (`master_in_charge_id`) REFERENCES `masters_in_charge` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table school_sports.sports: ~0 rows (approximately)
DELETE FROM `sports`;
/*!40000 ALTER TABLE `sports` DISABLE KEYS */;
INSERT INTO `sports` (`id`, `title`, `hpe_teacher_id`, `master_in_charge_id`) VALUES
	(1, 'Cricket', 1, 1);
/*!40000 ALTER TABLE `sports` ENABLE KEYS */;

-- Dumping structure for table school_sports.sport_age_groups
DROP TABLE IF EXISTS `sport_age_groups`;
CREATE TABLE IF NOT EXISTS `sport_age_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sport_id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `master_in_charge_id` int(11) unsigned NOT NULL,
  `coach_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sport_age_groups_sports` (`sport_id`),
  KEY `FK_sport_age_groups_masters_in_charge` (`master_in_charge_id`),
  KEY `FK_sport_age_groups_coaches` (`coach_id`),
  CONSTRAINT `FK_sport_age_groups_coaches` FOREIGN KEY (`coach_id`) REFERENCES `coaches` (`id`),
  CONSTRAINT `FK_sport_age_groups_masters_in_charge` FOREIGN KEY (`master_in_charge_id`) REFERENCES `masters_in_charge` (`id`),
  CONSTRAINT `FK_sport_age_groups_sports` FOREIGN KEY (`sport_id`) REFERENCES `sports` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table school_sports.sport_age_groups: ~0 rows (approximately)
DELETE FROM `sport_age_groups`;
/*!40000 ALTER TABLE `sport_age_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `sport_age_groups` ENABLE KEYS */;

-- Dumping structure for table school_sports.students
DROP TABLE IF EXISTS `students`;
CREATE TABLE IF NOT EXISTS `students` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admission_number` varchar(10) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `grade` varchar(10) NOT NULL DEFAULT '',
  `house` varchar(50) DEFAULT NULL,
  `date_of_admission` date NOT NULL,
  `bc_number` varchar(20) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contact_number` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `guardian_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table school_sports.students: ~20 rows (approximately)
DELETE FROM `students`;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` (`id`, `admission_number`, `full_name`, `dob`, `grade`, `house`, `date_of_admission`, `bc_number`, `email`, `contact_number`, `address`, `guardian_name`) VALUES
	(1, '12325', 'David Birla', '2004-06-12', '11C', 'Bonel', '2010-01-01', '4567', 'david@hello.com', '077845123', '#23/4, New Hospital Road, \nBatticaloa', 'Badshaa'),
	(2, '45124', 'Kumar Sangakara', '2005-09-16', '11A', 'Miller', '2011-01-13', '8451', 'gates_b@gmail.com', '0778745124', 'Main Street,\nColombo 12', 'Bill Gates'),
	(3, '20086', 'Sivarajakumar Thujiththiran', '2001-10-18', '13R', 'Bonel', '2012-01-08', '3131', 'thujiththiran@gmail.com', '0771234567', 'Nochchimunai, Navatkudah', 'Sivarajakumar'),
	(4, '18946', 'Jeyaraman Sujithrathan', '2002-06-30', '13Maths', 'Marian', '2008-01-16', '6167', 'sujithrathan@gmail.com', '0771234568', 'Hospital Road, Batticaloa', 'Jeyaraman'),
	(5, '22254', 'Alagaraj Latharshan', '2003-07-23', '12Arts', 'Crowther', '2020-10-02', '884', 'latharshan@gmail.com', '0771234569', 'Nochchimunai, Navatkudah', 'Alagaraj'),
	(6, '21784', 'Jeganathan Thavaviruthan', '2001-09-20', '13R', 'Miller', '2018-06-07', '1733', 'thavaviruthan@gmail.com', '0779876543', 'Kallady, Batticaloa', 'Jeganathan'),
	(7, '22241', 'Javanaraj Abilashan', '2003-03-09', '12Arts', 'Bonel', '2020-08-28', '9434', 'abilashan@gmail.com', '0779876542', 'Kallady, Batticaloa', 'Javanaraj'),
	(8, '19416', 'Mohan Mathisa Banuman', '2004-07-03', '11R', 'Marian', '2010-01-04', '865', 'mathisabanuman@gmail.com', '0779876541', 'Hospital Road, Batticaloa', 'Mohan'),
	(9, '21790', 'Sumithran Susikaran', '2001-11-17', '13R', 'Crowther', '2018-10-29', '3788', 'susikaran@gmail.com', '0761234567', 'Kallady, Batticaloa', 'Sumithran'),
	(10, '18852', 'Sasikumar Joy', '2002-12-22', '13Com', 'Miller', '2008-01-05', '8564', 'joy@gmail.com', '0761234568', 'Iruthayapuram, Batticaloa', 'Sasikumar'),
	(11, '18819', 'Anthonippillai Yathushan', '2003-01-07', '13Com', 'Bonel', '2008-01-16', '9056', 'yathushan@gmail.com', '0761234569', 'Iruthayapuram, Batticaloa', 'Anthonippillai'),
	(12, '21781', 'Sivakumar Rubikaran', '2001-10-10', '13R', 'Marian', '2018-06-07', '2502', 'rubikaran@gmail.com', '0769876543', 'Beach Road, Kallady', 'Sivakaran'),
	(13, '19108', 'Revon Ragel', '2003-06-06', '12Com', 'Crowther', '2009-01-05', '422', 'revon@gmail.com', '0769876542', 'Koolavady, Batticaloa', 'Ragel'),
	(14, '19396', 'Starrack Antony Glann', '2004-08-17', '11R', 'Miller', '2010-01-04', '6380', 'antonyglann@gmail.com', '0769876541', 'Puliyantheevu, Batticaloa', 'Starrack'),
	(15, '19928', 'Nadesamoorthy Jathurshanan', '2001-11-20', '13R', 'Bonel', '2006-01-01', '3811', 'jathurshanan@gmail.com', '0741234567', 'Navatkudah, Batticaloa', 'Nadesamoorthy'),
	(16, '19811', 'Sivapatham Niron', '2001-09-20', '13R', 'Marian', '2007-01-06', '3124', 'niron@gmail.com', '0741234568', 'Kallady, Batticaloa', 'Sivapatham'),
	(17, '18073', 'John Christopher Fernando Roshan', '2003-07-04', '12Com', 'Crowther', '2009-01-05', '805', 'roshan@gmail.com', '0741234569', 'Puthunagar, Batticaloa', 'John Christopher Fernando'),
	(18, '19705', 'Mariyanesan Sharon', '2005-10-20', '11D', 'Miller', '2011-01-04', '1573', 'sharon@gmail.com', '0749876543', 'Iruthayapuram, Batticaloa', 'Mariyanesan'),
	(19, '19885', 'Sivanathan Danistan', '2002-03-14', '13Com', 'Bonel', '2007-01-20', '4825', 'danishtan@gmail.com', '0749876542', 'Periyaurani, Batticaloa', 'Sivanathan'),
	(20, '19889', 'Kukathasan Shashanth', '2002-06-04', '13Com', 'Marian', '2007-01-05', '5876', 'shashanth', '0749876541', 'Iruthayapuram, Batticaloa', 'Kukathasan'),
	(22, '9999', 'Mango Banana', '2010-10-13', '6', 'Bonel', '2020-01-02', '4125', 'gm@hello.com', '07787451201', '#12, Main Street,\nBatticaloa', 'Mango G');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;

-- Dumping structure for table school_sports.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password_hash` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `role` enum('ADMIN','MANAGER','USER') NOT NULL DEFAULT 'USER',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table school_sports.users: ~0 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password_hash`, `email`, `full_name`, `role`) VALUES
	(1, 'admin', '$2y$10$JN/JQbRZ8zj6ReU5StNgc.AXIWuw7c8OexEk1Hlnh7/TBkuDzdyp2', 'admin@hello.com', 'Administrator', 'ADMIN'),
	(2, 'manager', '$2y$10$0W1eZw3JdxuC74WazYKvAOXXAdlzHrCkVoJ1e7a0JqM49qT1PYy/6', 'manager@hello.com', 'Manager Mango', 'MANAGER');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table school_sports.users_auth_keys
DROP TABLE IF EXISTS `users_auth_keys`;
CREATE TABLE IF NOT EXISTS `users_auth_keys` (
  `user_id` int(11) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `valid_till` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`user_id`),
  CONSTRAINT `FK_users_auth_keys_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table school_sports.users_auth_keys: ~1 rows (approximately)
DELETE FROM `users_auth_keys`;
/*!40000 ALTER TABLE `users_auth_keys` DISABLE KEYS */;
INSERT INTO `users_auth_keys` (`user_id`, `auth_key`, `valid_till`) VALUES
	(1, 'ad64611e91ac5a0f4fceb9cda19f39f5', '2022-04-03 06:33:13'),
	(2, 'c2189ba7a12e0adf3d1cd41859045ce4', '2022-04-11 12:16:06');
/*!40000 ALTER TABLE `users_auth_keys` ENABLE KEYS */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
