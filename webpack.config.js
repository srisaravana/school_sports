const path = require('path');

module.exports = {
    resolve: {
        extensions: ['.js', '.css', '.vue', '.jpg', '.svg', '.png'],
        alias: {
            '@src': path.resolve(__dirname, './vue'),
        }
    }
};
