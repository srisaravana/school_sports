# School Sports

### How to setup

1. Once cloned the repo, make sure the public folder inside the project is pointed as serving folder for the http server.
2. Run the SQL file in the project to set up the user table with admin user for login/authentication.
3. bootstrap.php file contains the connection details for the database server. Update the username, password and database name if needed.
4. run `npm install` to set up Vue building workflow. The project comes with Laravel-mix for building Vue SPA. After running `npm install` you can use `npx mix` to build Vue code, or use `npx mix watch` to run watch process that watch for any source code modification and build on the fly. more details [here.](https://laravel-mix.com/docs/6.0/upgrade#update-your-npm-scripts)



### Login details

Once you run the SQL file, you will have a single user (Administrator) in the database.

You can login to the app using the following credentials.

`username: admin`

`password: admin`

## Database Structure

```
students
- id
- admission_number (req)
- full_name
- age
- dob
- grade
- house
- date_of_admission
- bc_number
- email
- contact_number
- address
- guardian_name

hpe_teachers
- id
- full_name
- contact_number
- email
- address

masters_in_charge
- id
- full_name
- contat_number
- email
- address

coaches
- id
- full_name
- contact_number
- email
- address
- coaching_license (req)

sports
- id
- title
- hpe_teacher_id (fk -> hpe_teachers)
- master_in_charge_id (fk -> masters_in_charge)

sport_age_groups
- id
- sport_id (fk -> sports)
- title
- master_in_charge_id (fk -> masters_in_charge)
- coach_id (fk -> coaches)

sport_age_group_students
- id
- game_age_group_id (fk -> game_age_groups)
- student_id (fk -> students)

sport_practices
- id
- game_age_group_id (fk -> game_age_groups)
- place
- monday
- tuesday
- wednesday
- thursday
- friday
- saturday


sport_practice_dates [ignore]
- id
- sport_practices_id
- day [SUN,MON,TUE,WED,THU,FRI,SAT]
- time	

sport_places
- id
- sport_id
- place

matches
- id
- game_age_group_id (fk -> game_age_groups)
- master_in_charge_id (fk -> masters_in_charge)
- coach_id (fk -> coaches)
- venue
- date
- level [DIVISIONAL, ZONAL, PROVINCIAL, NATIONAL]
- type [SCHOOLS, ASSOCIATIONS, OTHERS]
- outcome [WIN, LOSS, DRAW]
- place [1ST, 2ND, 3RD, OTHER]

sport_camps
- id
- game_age_group_id (fk -> game_age_groups)
- coacher
- venue
- date

users
- id
- username
- email
- password_hash
- type [ADMIN, MANAGER, USER]

```
