<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Cricket\CricketAgeGroup;
use App\Models\Cricket\CricketAgeGroupMatchT20;

require_once '../../../../bootstrap.php';

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'id' => Request::getAsInteger( 'id', true ),
    ];


    $cricketAgeGroup = CricketAgeGroup::find( $fields[ 'id' ] );
    $stats = CricketAgeGroupMatchT20::getStats( $cricketAgeGroup );


    JSONResponse::validResponse( $stats );
    return;

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
