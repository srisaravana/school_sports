<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Cricket\CricketAgeGroupMatchT20;

require_once '../../../../bootstrap.php';

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'id' => Request::getAsInteger('id', true),

        'tournament_name' => Request::getAsString('tournament_name', true),
        'match_date' => Request::getAsString('match_date', true),
        'ground' => Request::getAsString('ground', true),

        'a_runs' => Request::getAsInteger('a_runs', true),
        'a_wickets' => Request::getAsInteger('a_wickets', true),

        'b_team_name' => Request::getAsString('b_team_name', true),
        'b_runs' => Request::getAsInteger('b_runs', true),
        'b_wickets' => Request::getAsInteger('b_wickets', true),

        'result' => Request::getAsString('result', true),
        'status' => Request::getAsString('status', true),
    ];

    $performance = CricketAgeGroupMatchT20::build($fields);

    $result = $performance->update();

    if ($result) {
        JSONResponse::validResponse();
        return;
    }

    throw new Exception('Failed to update T20 match details');
} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
