<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Cricket\CricketAgeGroupMatch2Days;

require_once '../../../../bootstrap.php';

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'age_group_id' => Request::getAsInteger( 'age_group_id', true ),

        'tournament_name' => Request::getAsString( 'tournament_name', true ),
        'match_date' => Request::getAsString( 'match_date', true ),
        'ground' => Request::getAsString( 'ground', true ),
        'b_team_name' => Request::getAsString( 'b_team_name', true ),
    ];

    $performance = CricketAgeGroupMatch2Days::build( $fields );

    $result = $performance->insert();

    if ( $result !== -1 ) {
        JSONResponse::validResponse();
        return;
    }

    throw new Exception( 'Failed to add age group performance' );

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
