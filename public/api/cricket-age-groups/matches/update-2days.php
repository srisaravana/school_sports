<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Cricket\CricketAgeGroupMatch2Days;

require_once '../../../../bootstrap.php';

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'id' => Request::getAsInteger( 'id', true ),

        'tournament_name' => Request::getAsString( 'tournament_name', true ),
        'match_date' => Request::getAsString( 'match_date', true ),
        'ground' => Request::getAsString( 'ground', true ),

        'a_runs_1' => Request::getAsInteger( 'a_runs_1', true ),
        'a_runs_2' => Request::getAsInteger( 'a_runs_2', true ),
        'a_wickets_1' => Request::getAsInteger( 'a_wickets_1', true ),
        'a_wickets_2' => Request::getAsInteger( 'a_wickets_2', true ),

        'b_team_name' => Request::getAsString( 'b_team_name', true ),
        'b_runs_1' => Request::getAsInteger( 'b_runs_1', true ),
        'b_runs_2' => Request::getAsInteger( 'b_runs_2', true ),
        'b_wickets_1' => Request::getAsInteger( 'b_wickets_1', true ),
        'b_wickets_2' => Request::getAsInteger( 'b_wickets_2', true ),

        'result' => Request::getAsString( 'result', true ),
        'status' => Request::getAsString( 'status', true ),
    ];

    $match = CricketAgeGroupMatch2Days::build( $fields );

    $result = $match->update();

    if ( $result ) {
        JSONResponse::validResponse();
        return;
    }

    throw new Exception( 'Failed to update 2days match details' );
} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
