<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Cricket\CricketAgeGroup;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'id' => Request::getAsInteger( 'id', true ),
        'title' => Request::getAsString( 'title', true ),
        'year' => Request::getAsInteger( 'year', true ),
        'age' => Request::getAsInteger( 'age', true ),
        'master_in_charge_id' => Request::getAsInteger( 'master_in_charge_id', true ),
        'coach_id' => Request::getAsInteger( 'coach_id', true ),
    ];

    $ageGroup = CricketAgeGroup::build( $fields );

    $result = $ageGroup->update();

    if ( $result ) {
        JSONResponse::validResponse();
        return;
    }

    throw new Exception( 'Failed to update.' );


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
