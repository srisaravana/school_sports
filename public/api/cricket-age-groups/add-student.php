<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Cricket\CricketAgeGroupStudent;

require_once '../../../bootstrap.php';

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'cricket_age_group_id' => Request::getAsInteger( 'cricket_age_group_id', true ),
        'student_id' => Request::getAsInteger( 'student_id', true ),
    ];

    $cricketAgeGroupStudent = CricketAgeGroupStudent::build( $fields );

    $result = $cricketAgeGroupStudent->insert();

    if ( $result !== -1 ) {
        JSONResponse::validResponse();
        return;
    }

    throw new Exception( 'Failed to add student' );

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
