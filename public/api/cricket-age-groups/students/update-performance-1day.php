<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Cricket\CricketStudentPerformance1Day;

require_once '../../../../bootstrap.php';

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'id' => Request::getAsInteger( 'id', true ),
//        'match_id' => Request::getAsInteger( 'match_id', true ),
//        'performance_type' => Request::getAsString( 'performance_type', true ),
        'batting_runs' => Request::getAsInteger( 'batting_runs' ),
        'batting_balls_faced' => Request::getAsInteger( 'batting_balls_faced' ),
        'batting_4s' => Request::getAsInteger( 'batting_4s' ),
        'batting_6s' => Request::getAsInteger( 'batting_6s' ),

        'bowling_overs' => Request::getAsFloat( 'bowling_overs' ),
        'bowling_balls' => Request::getAsInteger( 'bowling_balls' ),
        'bowling_runs' => Request::getAsInteger( 'bowling_runs' ),
        'bowling_wickets' => Request::getAsInteger( 'bowling_wickets' ),
        'bowling_wides' => Request::getAsInteger( 'bowling_wides' ),
        'bowling_noballs' => Request::getAsInteger( 'bowling_noballs' ),
    ];

    $studentPerformance = CricketStudentPerformance1Day::build( $fields );

    $result = $studentPerformance->update();

    if ( $result ) {
        JSONResponse::validResponse();
        return;
    }

    throw new Exception( 'Failed to update performance details' );

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
