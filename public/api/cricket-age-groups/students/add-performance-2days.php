<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Cricket\CricketStudentPerformance2Days;

require_once '../../../../bootstrap.php';

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'student_id' => Request::getAsInteger( 'student_id', true ),
        'match_id' => Request::getAsInteger( 'match_id', true ),
        'performance_type' => Request::getAsString( 'performance_type', true ),

        'batting_runs_1' => Request::getAsInteger( 'batting_runs_1' ),
        'batting_runs_2' => Request::getAsInteger( 'batting_runs_2' ),
        'batting_balls_faced_1' => Request::getAsInteger( 'batting_balls_faced_1' ),
        'batting_balls_faced_2' => Request::getAsInteger( 'batting_balls_faced_2' ),
        'batting_4s_1' => Request::getAsInteger( 'batting_4s_1' ),
        'batting_4s_2' => Request::getAsInteger( 'batting_4s_2' ),
        'batting_6s_1' => Request::getAsInteger( 'batting_6s_1' ),
        'batting_6s_2' => Request::getAsInteger( 'batting_6s_2' ),

        'bowling_overs_1' => Request::getAsFloat( 'bowling_overs_1' ),
        'bowling_overs_2' => Request::getAsFloat( 'bowling_overs_2' ),
        'bowling_balls_1' => Request::getAsInteger( 'bowling_balls_1' ),
        'bowling_balls_2' => Request::getAsInteger( 'bowling_balls_2' ),
        'bowling_runs_1' => Request::getAsInteger( 'bowling_runs_1' ),
        'bowling_runs_2' => Request::getAsInteger( 'bowling_runs_2' ),
        'bowling_wickets_1' => Request::getAsInteger( 'bowling_wickets_1' ),
        'bowling_wickets_2' => Request::getAsInteger( 'bowling_wickets_2' ),
        'bowling_wides_1' => Request::getAsInteger( 'bowling_wides_1' ),
        'bowling_wides_2' => Request::getAsInteger( 'bowling_wides_2' ),
        'bowling_noballs_1' => Request::getAsInteger( 'bowling_noballs_1' ),
        'bowling_noballs_2' => Request::getAsInteger( 'bowling_noballs_2' ),
    ];

    $studentPerformance = CricketStudentPerformance2Days::build( $fields );

    $result = $studentPerformance->insert();

    if ( $result !== -1 ) {
        $studentPerformance = CricketStudentPerformance2Days::find( $result );
        JSONResponse::validResponse( $studentPerformance );
        return;
    }

    throw new Exception( 'Failed to add performance details' );

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
