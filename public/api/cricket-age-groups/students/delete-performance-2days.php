<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Cricket\CricketStudentPerformance1Day;
use App\Models\Cricket\CricketStudentPerformance2Days;

require_once '../../../../bootstrap.php';

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'id' => Request::getAsInteger( 'id', true ),
    ];

    $studentPerformance = CricketStudentPerformance2Days::build( $fields );

    $result = $studentPerformance->delete();

    if ( $result ) {
        JSONResponse::validResponse();
        return;
    }

    throw new Exception( 'Failed to delete performance details' );

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
