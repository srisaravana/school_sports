<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Cricket\CricketStudentPerformance1Day;
use App\Models\Cricket\CricketStudentPerformanceT20;
use App\Models\Student;

require_once '../../../../bootstrap.php';

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'student_id' => Request::getAsInteger( 'student_id', true ),
    ];

    $student = Student::find( $fields[ 'student_id' ] );

    if ( empty( $student ) ) throw new Exception( 'Invalid student' );

    $performances = CricketStudentPerformanceT20::findByStudent( $student );

    JSONResponse::validResponse( $performances );
    return;


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
