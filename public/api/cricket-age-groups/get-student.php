<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Cricket\CricketAgeGroupStudent;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();

    /*
     * Get cricket age group student by id
     * */

    $id = Request::getAsInteger( "id", true );

    $ageGroupStudent = CricketAgeGroupStudent::find( $id );
    if ( empty( $ageGroupStudent ) ) throw new Exception( "Invalid age group student" );

    JSONResponse::validResponse( $ageGroupStudent );
    return;


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
