<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Cricket\CricketAgeGroup;

require_once '../../../bootstrap.php';

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'title' => Request::getAsString( 'title', true ),
        'year' => Request::getAsInteger( 'year', true ),
        'age' => Request::getAsInteger( 'age', true ),
        'master_in_charge_id' => Request::getAsInteger( 'master_in_charge_id', true ),
        'coach_id' => Request::getAsInteger( 'coach_id', true ),
    ];
    
    $ageGroup = CricketAgeGroup::build( $fields );

    $result = $ageGroup->insert();

    if ( $result !== -1 ) {
        $ageGroup = CricketAgeGroup::find( $result );
        JSONResponse::validResponse( $ageGroup );
        return;
    }

    throw new Exception( 'Failed to create new age group' );

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
