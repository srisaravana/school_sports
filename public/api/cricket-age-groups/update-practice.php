<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Cricket\CricketPractice;

require_once "../../../bootstrap.php";

try {

    Auth::authenticate();


    $fields = [
        "id" => Request::getAsInteger( "id", true ),
        "place" => Request::getAsString( "place", true ),
        "monday" => Request::getAsString( "monday", true ),
        "tuesday" => Request::getAsString( "tuesday", true ),
        "wednesday" => Request::getAsString( "wednesday", true ),
        "thursday" => Request::getAsString( "thursday", true ),
        "friday" => Request::getAsString( "friday", true ),
        "saturday" => Request::getAsString( "saturday", true ),
        "sunday" => Request::getAsString( "sunday", true ),
    ];

    $practice = CricketPractice::build( $fields );
    $result = $practice->update();

    if ( $result ) {
        JSONResponse::validResponse( "Updated" );
        return;
    }

    throw new Exception( "Failed to update" );


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
