<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request as Request;
use App\Core\Services\ImageProcessor;
use App\Core\Services\Uploader;
use App\Core\Resources\MimeTypes;
use App\Models\User;

require_once "../../../bootstrap.php";

$MAX_UPLOAD_SIZE = 1.0 * 1024 * 1024; // 5 MB in bytes

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();

    $file = Request::getFile( "file" );

    if ( empty( $file ) ) throw new Exception( "No file uploaded" );

    $uploader = new Uploader( $file, $MAX_UPLOAD_SIZE, "images/a/b/c", MimeTypes::IMAGE_MIMES );


    $processor = ImageProcessor::createImageObject( $uploader );
    $processor->resizeByWidth( 200 )->save( "hello.jpg" );

    $path = $processor->getRelativePath();


    /* return stored relative path back as response */
    JSONResponse::validResponse( $path );
    return;


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
