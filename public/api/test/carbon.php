<?php
declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request as Request;
use App\Core\Services\ImageProcessor;
use App\Core\Services\Uploader;
use App\Core\Resources\MimeTypes;
use App\Models\User;
use Carbon\Carbon;

require_once "../../../bootstrap.php";

$age = 20;

$today = (new Carbon())->startOfYear();
$startYear = $today->subYears($age)->toDateString(); // 20

$today = (new Carbon())->startOfYear();
$endYear = $today->subYears($age - 2)->toDateString(); // 21

JSONResponse::validResponse([
    'today' => $today,
    'start' => $startYear,
    'end' => $endYear,
]);
