<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Event;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "title" => Request::getAsString("title", true),
        "hpe_teacher_id" => Request::getAsInteger("hpe_teacher_id", true),
        "master_in_charge_id" => Request::getAsInteger("master_in_charge_id", true),
    ];


    $sport = Event::build($fields);

    $result = $sport->insert();

    if ($result) {

        $sport = Event::find($result);

        JSONResponse::validResponse(["sport" => $sport]);
        return;
    }


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
