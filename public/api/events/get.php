<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Event;

require_once "../../../bootstrap.php";

try {

    Auth::authenticate();

    $id = Request::getAsInteger("id", true);
    $sport = Event::find($id);

    if (is_null($sport)) throw new Exception("Invalid sport");

    JSONResponse::validResponse($sport);
    return;

} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
