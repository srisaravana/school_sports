<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Athletics\RunningGame;

require_once "../../../bootstrap.php";

try {

    Auth::authenticate();

    $id = Request::getAsInteger( "id", true );
    $game = RunningGame::find( $id );

    if ( is_null( $game ) ) throw new Exception( "Invalid game" );

    JSONResponse::validResponse( $game );
    return;

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
