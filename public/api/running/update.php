<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Athletics\RunningGame;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "id" => Request::getAsInteger( "id", true ),
        "title" => Request::getAsString( "title", true ),
        "hpe_teacher_id" => Request::getAsInteger( "hpe_teacher_id", true ),
        "master_in_charge_id" => Request::getAsInteger( "master_in_charge_id", true ),
    ];


    $game = RunningGame::find( $fields[ "id" ] );

    if ( empty( $game ) ) throw new Exception( "Invalid game id" );

    $games = RunningGame::build( $fields );

    $result = $games->update();

    if ( $result ) {
        JSONResponse::validResponse( "Updated" );
        return;
    }

    throw new Exception( "Failed to update" );


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
