<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Core\Resources\MimeTypes;
use App\Core\Services\ImageProcessor;
use App\Core\Services\Storage;
use App\Core\Services\Uploader;
use App\Models\Staff\Coach;
use App\Models\Staff\MasterInCharge;

require_once "../../../../bootstrap.php";

try {

    Auth::authenticate();

    $fields = [
        "id" => Request::getAsInteger( "id", true ),
        "profile_pic" => Request::getFile( "profile_pic" ),
    ];

    $masterInCharge = MasterInCharge::find( $fields[ "id" ] );
    if ( empty( $masterInCharge ) ) throw new Exception( "Invalid master in-charge" );

    $uploader = new Uploader( $fields[ "profile_pic" ], Storage::getMaxUploadSize(), "uploads/images/mic", MimeTypes::IMAGE_MIMES );

    $processor = ImageProcessor::createImageObject( $uploader );
    $processor->fit( 200 )->save( "mic_" . $masterInCharge->id . ".jpg", 80, "jpg" );

    $path = $processor->getRelativePath();

    $masterInCharge->profile_pic = $path;

    if ( $masterInCharge->updateProfilePicture() ) {
        JSONResponse::validResponse();
        return;
    }

    throw new Exception( "Failed to upload profile picture" );


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
