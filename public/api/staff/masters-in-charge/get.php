<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Staff\MasterInCharge;

require_once "../../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $id = Request::getAsInteger("id", true);


    $teacher = MasterInCharge::find($id);

    if (is_null($teacher)) throw new Exception("Invalid teacher");

    JSONResponse::validResponse($teacher);
    return;


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
