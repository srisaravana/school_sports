<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Core\Resources\MimeTypes;
use App\Core\Services\ImageProcessor;
use App\Core\Services\Uploader;
use App\Core\Services\Storage;
use App\Models\Staff\HpeTeacher;

require_once "../../../../bootstrap.php";

try {

    Auth::authenticate();

    $fields = [
        "id" => Request::getAsInteger( "id", true ),
        "profile_pic" => Request::getFile( "profile_pic" ),
    ];

    $hpeTeacher = HpeTeacher::find( $fields["id"] );
    if ( empty( $hpeTeacher ) ) throw new Exception( "Invalid student" );

    $uploader = new Uploader( $fields["profile_pic"], Storage::getMaxUploadSize(), "uploads/images/hpe-teachers", MimeTypes::IMAGE_MIMES );

    $processor = ImageProcessor::createImageObject( $uploader );
    $processor->fit( 200 )->save( "hpe_" . $hpeTeacher->id . ".jpg", 80, "jpg" );

    $path = $processor->getRelativePath();

    $hpeTeacher->profile_pic = $path;

    if ( $hpeTeacher->updateProfilePicture() ) {
        JSONResponse::validResponse();
        return;
    }

    throw new Exception( "Failed to upload profile picture" );


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
