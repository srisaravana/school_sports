<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Staff\HpeTeacher;

require_once "../../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "id" => Request::getAsInteger("id", true),
        "full_name" => Request::getAsString("full_name", true),
        "contact_number" => Request::getAsString("contact_number", true),
        "email" => Request::getAsString("email", true),
        "address" => Request::getAsString("address", true),
    ];


    if (empty($fields["id"]) || empty($fields["full_name"]))
        throw new Exception("Required values missing");


    $teacher = HpeTeacher::find($fields["id"]);

    if (empty($teacher)) throw new Exception("Invalid teacher id");

    $teacher = HpeTeacher::build($fields);

    $result = $teacher->update();

    if ($result) {
        JSONResponse::validResponse("Updated");
        return;
    }

    throw new Exception("Failed to update");


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
