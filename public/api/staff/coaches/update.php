<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Staff\Coach;

require_once "../../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "id" => Request::getAsInteger("id", true),
        "full_name" => Request::getAsString("full_name", true),
        "contact_number" => Request::getAsString("contact_number", true),
        "email" => Request::getAsString("email", true),
        "address" => Request::getAsString("address", true),
        "coaching_license" => Request::getAsString("coaching_license", true),
    ];


    if (empty($fields["id"]) || empty($fields["full_name"]))
        throw new Exception("Required values missing");

    $coach = Coach::find($fields["id"]);

    if (empty($coach)) throw new Exception("Invalid coach id");

    $coach = Coach::build($fields);

    $result = $coach->update();

    if ($result) {
        JSONResponse::validResponse("Updated");
        return;
    }

    throw new Exception("Failed to update");

} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
