<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Core\Resources\MimeTypes;
use App\Core\Services\ImageProcessor;
use App\Core\Services\Storage;
use App\Core\Services\Uploader;
use App\Models\Staff\Coach;

require_once "../../../../bootstrap.php";

try {

    Auth::authenticate();

    $fields = [
        "id" => Request::getAsInteger( "id", true ),
        "profile_pic" => Request::getFile( "profile_pic" ),
    ];

    $coach = Coach::find( $fields[ "id" ] );
    if ( empty( $coach ) ) throw new Exception( "Invalid coach" );

    $uploader = new Uploader( $fields[ "profile_pic" ], Storage::getMaxUploadSize(), "uploads/images/coaches", MimeTypes::IMAGE_MIMES );

    $processor = ImageProcessor::createImageObject( $uploader );
    $processor->fit( 200 )->save( "coach_" . $coach->id . ".jpg", 80, "jpg" );

    $path = $processor->getRelativePath();

    $coach->profile_pic = $path;

    if ( $coach->updateProfilePicture() ) {
        JSONResponse::validResponse();
        return;
    }

    throw new Exception( "Failed to upload profile picture" );


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
