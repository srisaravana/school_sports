<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Staff\Coach;

require_once "../../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "full_name" => Request::getAsString("full_name", true),
        "contact_number" => Request::getAsString("contact_number", true),
        "email" => Request::getAsString("email", true),
        "address" => Request::getAsString("address", true),
        "coaching_license" => Request::getAsString("coaching_license", true),
    ];

    if (empty($fields["full_name"]) || empty($fields["contact_number"]))
        throw new Exception("Required fields are missing data");

    $coach = Coach::build($fields);

    $result = $coach->insert();

    if ($result) {

        $coach = Coach::find($result);

        JSONResponse::validResponse($coach);
        return;
    }


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
