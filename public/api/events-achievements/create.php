<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\AgeGroupAchievement;

require_once '../../../bootstrap.php';

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'age_group_id' => Request::getAsInteger( 'age_group_id', true ),
        'tournament_name' => Request::getAsString( 'tournament_name', true ),
        'tournament_date' => Request::getAsString( 'tournament_date', true ),
        'outcome' => Request::getAsString( 'outcome', true ),
        'game_data' => Request::getAsString( 'game_data', true ),
    ];


    $achievement = AgeGroupAchievement::build( $fields );

    $result = $achievement->insert();

    if ( $result !== -1 ) {

        JSONResponse::validResponse( $achievement );
        return;
    }

    throw new Exception( 'Failed to add achievement' );


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
