<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\AgeGroupAchievement;

require_once '../../../bootstrap.php';

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'id' => Request::getAsInteger( 'id', true ),
    ];

    $achievement = AgeGroupAchievement::build( $fields );

    $result = $achievement->delete();

    if ( $result ) {

        JSONResponse::validResponse( 'Deleted' );
        return;
    }

    throw new Exception( 'Failed to delete achievement' );


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
