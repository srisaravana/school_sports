<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\AgeGroupAchievement;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    /* age_group_id */
    $id = Request::getAsInteger( "id", true );

    $achievements = AgeGroupAchievement::findBySportAgeGroup( $id );

    JSONResponse::validResponse( $achievements );
    return;


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
