<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Athletics\RunningAgeGroup;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "running_game_id" => Request::getAsInteger( "running_game_id", true ),
        "title" => Request::getAsString( "title", true ),
        "year" => Request::getAsInteger( "year", true ),
    ];


    $games = RunningAgeGroup::build( $fields );

    $result = $games->insert();

    if ( $result ) {
        JSONResponse::validResponse();
        return;
    }


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
