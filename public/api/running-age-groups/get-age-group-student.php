<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Athletics\RunningAgeGroupStudent;

require_once "../../../bootstrap.php";

try {

    Auth::authenticate();

    $id = Request::getAsInteger( "id", true );
    $student = RunningAgeGroupStudent::find( $id );

    if ( is_null( $student ) ) throw new Exception( "Invalid student" );

    JSONResponse::validResponse( $student );
    return;

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
