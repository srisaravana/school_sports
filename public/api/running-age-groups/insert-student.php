<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Athletics\RunningAgeGroupStudent;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "student_id" => Request::getAsInteger( "student_id", true ),
        "age_group_id" => Request::getAsInteger( "age_group_id", true ),
    ];


    $games = RunningAgeGroupStudent::build( $fields );

    $result = $games->insert();

    if ( $result ) {
        JSONResponse::validResponse();
        return;
    }


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
