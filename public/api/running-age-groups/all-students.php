<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Athletics\RunningAgeGroup;
use App\Models\Athletics\RunningAgeGroupStudent;

require_once "../../../bootstrap.php";

try {

    Auth::authenticate();

    $id = Request::getAsInteger( "id", true );
    $ageGroup = RunningAgeGroup::find( $id );

    if ( is_null( $ageGroup ) ) throw new Exception( "Invalid ageGroup" );


    $students = RunningAgeGroupStudent::findByAgeGroup( $ageGroup );

    JSONResponse::validResponse( $students );
    return;

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
