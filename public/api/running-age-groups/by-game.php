<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Athletics\RunningAgeGroup;
use App\Models\Athletics\RunningGame;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $gameId = Request::getAsInteger( 'game_id', true );

    $runningGame = RunningGame::find( $gameId );

    if ( is_null( $runningGame ) ) throw new Exception( [ 'Invalid game' ] );

    $ageGroups = RunningAgeGroup::findByGame( $runningGame );

    JSONResponse::validResponse( $ageGroups );
    return;


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
