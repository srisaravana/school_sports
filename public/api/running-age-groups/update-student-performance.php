<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Athletics\RunningStudentPerformance;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "id" => Request::getAsInteger( "id", true ),
        "meet_date" => Request::getAsString( "meet_date", true ),
        "meet_title" => Request::getAsString( "meet_title", true ),
        "meet_venue" => Request::getAsString( "meet_venue", true ),
        "place" => Request::getAsString( "place", true ),
        "timing" => Request::getAsFloat( "timing", true ),
    ];


    $games = RunningStudentPerformance::build( $fields );

    $result = $games->update();

    if ( $result ) {
        JSONResponse::validResponse();
        return;
    }


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
