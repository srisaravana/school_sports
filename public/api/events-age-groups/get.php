<?php

declare( strict_types=1 );

use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\EventAgeGroup;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    // Auth::authenticate();


    $id = Request::getAsInteger( "id", true );

    $ageGroup = EventAgeGroup::find( $id );
    if ( empty( $ageGroup ) ) throw new Exception( "Invalid age group" );

    JSONResponse::validResponse( $ageGroup );
    return;


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
