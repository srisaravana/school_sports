<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Cricket\CricketAgeGroupStudent;
use App\Models\EventAgeGroup;
use App\Models\EventAgeGroupStudent;
use App\Models\Student;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();

    /*
     * Get cricket age group student by id
     * */

    $studentId = Request::getAsInteger("student_id", true);

    $student = Student::find($studentId);

    if (empty($student)) throw new Exception('Invalid student');

    $studentAgeGroups = EventAgeGroupStudent::findByStudent($student);

    JSONResponse::validResponse($studentAgeGroups);
    return;


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
