<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Event;
use App\Models\EventAgeGroup;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $id = Request::getAsInteger( "id", true );

    $event = Event::find( $id );

    if ( empty( $event ) ) throw new Exception( "Invalid event id" );

    $ageGroups = EventAgeGroup::findByEvent( $event );

    JSONResponse::validResponse( $ageGroups );
    return;


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
