<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\EventStudentPerformance;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "student_id" => Request::getAsInteger( "student_id", true ),
        "meet_date" => Request::getAsString( "meet_date", true ),
        "meet_title" => Request::getAsString( "meet_title", true ),
        "meet_venue" => Request::getAsString( "meet_venue", true ),
        "place" => Request::getAsString( "place", true ),
        "value" => Request::getAsFloat( "value", true ),
    ];


    $games = EventStudentPerformance::build( $fields );

    $result = $games->insert();

    if ( $result ) {
        JSONResponse::validResponse();
        return;
    }


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
