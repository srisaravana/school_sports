<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\EventAgeGroupStudent;
use App\Models\EventAgeGroup;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "student_id" => Request::getAsInteger( "student_id", true ),
        "age_group_id" => Request::getAsInteger( "age_group_id", true ),
    ];


    /* check if age group exist */
    $ageGroup = EventAgeGroup::find( $fields["age_group_id"] );
    if ( empty( $ageGroup ) ) throw new Exception( "Invalid age group" );

    /* check if student is already in the age group */
    if ( EventAgeGroupStudent::findByIds( $fields["age_group_id"], $fields["student_id"] ) ) {
        throw new Exception( "Student already exist in the age group" );
    }

    /* add the student into the age group */
    $ageGroupStudent = EventAgeGroupStudent::build( $fields );
    $result = $ageGroupStudent->insert();

    if ( $result ) {
        JSONResponse::validResponse();
        return;
    }

    throw new Exception( "Failed to add student to the age group" );


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
