<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\EventAgeGroup;

require_once '../../../bootstrap.php';

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'id' => Request::getAsInteger('id', true),
        'title' => Request::getAsString('title', true),
        'master_in_charge_id' => Request::getAsInteger('master_in_charge_id', true),
        'coach_id' => Request::getAsInteger('coach_id', true),
        'year' => Request::getAsInteger('year', true),
    ];


    $ageGroup = EventAgeGroup::find($fields['id']);

    if (empty($ageGroup)) throw new Exception('Invalid age group id');

    $ageGroup = EventAgeGroup::build($fields);

    $result = $ageGroup->update();

    if ($result) {
        JSONResponse::validResponse('Updated');
        return;
    }

    throw new Exception('Failed to update');


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
