<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\EventAgeGroupStudent;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "id" => Request::getAsInteger( "id", true ),
    ];


    /* remove the student from the age group */
    $ageGroupStudent = EventAgeGroupStudent::build( $fields );
    $result = $ageGroupStudent->delete();

    if ( $result ) {
        JSONResponse::validResponse();
        return;
    }

    throw new Exception( "Failed to remove student from the age group" );


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
