<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\EventAgeGroup;

require_once '../../../bootstrap.php';

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'title' => Request::getAsString('title', true),
        'sport_id' => Request::getAsInteger('sport_id', true),
        'master_in_charge_id' => Request::getAsInteger('master_in_charge_id', true),
        'coach_id' => Request::getAsInteger('coach_id', true),
        'year' => Request::getAsInteger('year', true),
    ];


    $ageGroup = EventAgeGroup::build($fields);

    /* creating a new age group */
    $result = $ageGroup->insert();

    if ($result !== -1) {

        $ageGroup = EventAgeGroup::find($result);

        JSONResponse::validResponse($ageGroup);
        return;
    }

    throw new Exception('Failed to create new age group');


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
