<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Student;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "id" => Request::getAsInteger("id"),
        "admission_number" => Request::getAsString("admission_number", true),
        "date_of_admission" => Request::getAsString("date_of_admission", true),

        "full_name" => Request::getAsString("full_name", true),
        "dob" => Request::getAsString("dob", true),
        "bc_number" => Request::getAsString("bc_number", true),

        "grade" => Request::getAsString("grade", true),
        "house" => Request::getAsString("house", true),

        "guardian_name" => Request::getAsString("guardian_name", true),
        "address" => Request::getAsString("address", true),
        "contact_number" => Request::getAsString("contact_number", true),
        "email" => Request::getAsString("email", true),
    ];


    if (empty($fields["id"]) || empty($fields["admission_number"]))
        throw new Exception("Required values missing");


    $student = Student::find($fields["id"]);

    if (empty($student)) throw new Exception("Invalid student id");

    $student = Student::build($fields);

    $result = $student->update();

    if ($result) {
        JSONResponse::validResponse("Updated");
        return;
    }

    throw new Exception("Failed to update");


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
